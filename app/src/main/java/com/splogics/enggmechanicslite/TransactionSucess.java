package com.splogics.enggmechanicslite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TransactionSucess extends AppCompatActivity
{
    private Bundle bundle = null;
    private TextView txtamount ,txttranID;
    private Button userOkay;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_sucess);

        txtamount = (TextView) findViewById(R.id.txtPrice);
        txttranID = (TextView) findViewById(R.id.txttrans_number) ;
        userOkay = (Button) findViewById(R.id.user_okay);
        userOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TransactionSucess.this,HomeActivity.class);
                startActivity(intent);
                finish();

            }
        });
        bundle = getIntent().getExtras();
        if(bundle != null)
        {
            Double amt = bundle.getDouble("amount");
            txtamount.setText(""+amt);

            String transID = bundle.getString("transId");
            txttranID.setText(transID);

        }
    }
}
