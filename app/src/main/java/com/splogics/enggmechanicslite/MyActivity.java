package com.splogics.enggmechanicslite;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.payUMoney.sdk.PayUmoneySdkInitilizer;
import com.payUMoney.sdk.SdkConstants;
import com.splogics.enggmechanicslite.WebService.InsertPaymentTrans;
import com.splogics.enggmechanicslite.WebService.UpdatePaymentTrans;
import com.splogics.enggmechanicslite.WebService.UpdateUserPaymentStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;


public class MyActivity extends Activity {


    public static MyActivity Obj;
    public static MyActivity getObject() {
        return Obj;
    }
    AlertDialog ad;
    private String UuserID, UEmail, UName, UPhonenumber, UAndroidID;
    EditText amt = null;
    Button pay = null;
    public static final String TAG = "PayUMoney";
    private SharedPreferences sharedPreferences_loggedUser;
    private TextView terms_condition;
    Double amount;
    String paymentId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        getActionBar().setTitle(R.string.payment);
        getActionBar().setIcon(R.drawable.logo);
        amt = (EditText) findViewById(R.id.amount);
        pay = (Button) findViewById(R.id.pay);
        pay.setEnabled(false);

        //intializing object

        Obj = this;

        popup_message_india_only();

        terms_condition = (TextView) findViewById(R.id.terms_condition_btn);
        terms_condition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                pop_up();
            }
        });

        // we need to get the Logeged  userDetails
        // webservice OR from storedProcedures


        sharedPreferences_loggedUser = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
        UEmail = sharedPreferences_loggedUser.getString("LoggedUser_Email", "null");
        UPhonenumber = sharedPreferences_loggedUser.getString("LoggedUser_Phone", "null");
        UName = sharedPreferences_loggedUser.getString("LoggedUser_Name", "null");
        UuserID = sharedPreferences_loggedUser.getString("LoggedUser_userID", "null");
        UAndroidID = sharedPreferences_loggedUser.getString("LoggedUser_AndroidID", "null");
        if (UEmail.equals("null") && UName.equals("null") && UPhonenumber.equals("null"))
        {
            Toast.makeText(getApplicationContext(), "Some Error", Toast.LENGTH_SHORT).show();
        }
        else
        {
            pay.setEnabled(true);
        }
    }

    public void popup_message_india_only()
    {
        AlertDialog.Builder alertadd = new AlertDialog.Builder(MyActivity.this);
        LayoutInflater factory = LayoutInflater.from(MyActivity.this);
        final View view = factory.inflate(R.layout.india_only, null);
        TextView txtmsg = (TextView) view.findViewById(R.id.txtmsg_indiaonly);
        Button btnokay =  (Button) view.findViewById(R.id.btnok_indianOnly);
        btnokay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();
            }
        });

        alertadd.setView(view);
        ad = alertadd.show();

    }


    public void pop_up()
    {

     AlertDialog.Builder alertadd = new AlertDialog.Builder(MyActivity.this);
        LayoutInflater factory = LayoutInflater.from(MyActivity.this);
        final View view = factory.inflate(R.layout.terms_conditions, null);
        TextView textViewshopname = (TextView) view.findViewById(R.id.agreement_textview);
        textViewshopname.setText(Html.fromHtml(
                "<h2><strong>Engineering Mechanics App </strong></h2>\n" +
                        "\n" +
                        "<p>Terms and Conditions<br />\n" +
                        "ABOUT THE APPLICATION:</p>\n" +
                        "\n" +
                        "<p>A warm welcome to our mobile application - Engineering Mechanics (the &ldquo;App&rdquo;). This App is developed and maintained by SP Logic Technologies (P) Ltd. Kochi, Kerala, India (the &ldquo;Company&rdquo;).&nbsp;</p>\n" +
                        "\n" +
                        "<p>THE APP PRIVACY POLICY:</p>\n" +
                        "\n" +
                        "<p>When you register with the Company and use the App, you will provide your name, email address, mobile number, transaction-related information, information you provide the Company when you contact the Company for help and credit card information for purchase. We collect device IDs for registration as well as for updates for better experience of the App in future. However, the Company assures the users that the data you provided as well as the data the App collected will not be used for any other purpose other than specified in the privacy policy of the App. We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorised access, disclosure, copying, use or modification.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "AGREEMENT:</p>\n" +
                        "\n" +
                        "<p>If you download or install the App on your device(s), you agree to the terms and conditions (the &ldquo;Terms&rdquo;) of the App and the privacy policy of the App. If you are not agreeing to the Terms or not satisfied with the privacy policy, you should immediately stop using it and should uninstall the App from your device(s). To help you with doubts and confusions, you can contact through our e-mail: logixsp@gmail.com. Any continued use of the App or services of the App means you agree to the Terms and satisfied by the privacy policy of the App.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "GENERAL CONDITIONS:</p>\n" +
                        "\n" +
                        "<p>The App is published and is available to you for your own personal use only. It should not be used for public or commercial purpose. It is strictly restricted to use the App for any illegal, fraudulent, unlawful or harmful purposes including (without limitation) copyright infringement. You are not supposed to use this app for any marketing purposes without the permission from the company. Also the user must comply with any applicable international laws, including the local laws in the user&rsquo;s country.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "CONTENT:</p>\n" +
                        "\n" +
                        "<p>The copyright in all content materials in the App including all information, data, text, audio, photographs, graphics and videos and other material (the &ldquo;Material&ldquo;) is owned by or licensed to the Company and all rights of the mentioned are reserved. You can view the Material for your own personal use but you cannot otherwise copy, modify, change, re-produce, re-publish, display publicly, re-distribute, save, exchange or commercially exploit in any form whatsoever or use the Material without the Company&rsquo;s explicit permission.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "THE DOs and DONTs:</p>\n" +
                        "\n" +
                        "<p>You agree that when using the App you will strictly adhere to all the applicable laws and these Terms. In particular, but without limitation, you agree not to:<br />\n" +
                        "Intentional attempt to get unauthorised access to the App or any networks, servers or computer systems directly or indirectly connected to the App.<br />\n" +
                        "Modify, adapt, translate or reverse engineer any part of the App or re-format or frame any portion of the pages displayed in the App, save to the extent expressly permitted by these Terms or by law.<br />\n" +
                        "You agree to indemnify the Company in full and on demand from and against any loss, damage, costs or expenses which they suffer or incur directly or indirectly as a result of your use of the App otherwise than in accordance with these Terms or applicable laws.<br />\n" +
                        "THE LOGOS: The logos (the &ldquo;Logos&ldquo;) used in the App are owned by the Company. You cannot use, copy, edit, vary, reproduce, publish, display, distribute, store, transmit, commercially exploit or disseminate the Logos without the prior written consent of the Company.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "LIMITATIONS OF LIABILITY:</p>\n" +
                        "\n" +
                        "<p>THE APP IS PROVIDED ON AN &ldquo;AS IS&rdquo; BASIS. USE OF THE APP IS AT YOUR OWN RISK. TO THE MAXIMUM EXTENT PERMITTED BY LAW:&nbsp;<br />\n" +
                        "(A) THE COMPANY DISCLAIMS ALL LIABILITY WHATSOEVER, WHETHER ARISING IN CONTRACT, NEGLIGENCE OR OTHERWISE IN RELATION TO THE APP; AND&nbsp;<br />\n" +
                        "(B) ALL IMPLIED WARRANTIES, TERMS AND CONDITIONS RELEVANT TO THE APP (WHETHER IMPLIED BY STATUE, COMMON LAW OR ANY OTHER), INCLUDING (WITHOUT LIMITATION) ANY WARRANTY, TERM OR CONDITION AS TO ACCURACY, PERFORMANCE, COMPLETENESS, SATISFACTORY QUALITY, FITNESS FOR ANY OR SPECIAL PURPOSE, AVAILABILITY, NON INFRINGEMENT, INFORMATION ACCURACY, INTEROPERABILITY, QUIET USAGE AND TITLE ARE, AS BETWEEN THE COMPANY AND YOU, HEREBY EXCLUDED. IN PARTICULAR, BUT WITHOUT PREJUDICE TO THE FOREGOING, THE COMPANY ACCEPT NO RESPONSIBILITY FOR ANY TECHNICAL FAILURE OF THE INTERNET AND/OR THE APP; OR ANY DAMAGE OR ECONOMIC LOSS (INCLUDING, WITHOUT LIMITATION, LOSS OF MONEY, PROFITS, OR INJURY TO USERS OR THEIR EQUIPMENT AS A RESULT OF OR RELATING TO THEIR USE OF THE APP. YOUR STATUTORY RIGHTS ARE NOT AFFECTED.&nbsp;<br />\n" +
                        "(C) IF THE COMPANY IS LIABLE TO YOU DIRECTLY OR INDIRECTLY IN RELATION TO THE APP, THAT LIABILITY (HOWSOEVER ARISING) IS LIMITED TO THE SUM PAID BY YOU AT THE TIME OF REGISTRATION OF THE APP, OR ANY IN-APP PURCHASE, INCLUDING SUBSCRIPTIONS, WHICHEVER IS GREATER.<br />\n" +
                        "(D) REFUND POLICY: NO REFUND WILL BE GIVEN UNDER ANY CIRCUMSTANCES TO ANY USER IF THE USER REGISTERS AND USES THE CONTENT OF THE APPLICATION AS THE APP CONTAINS DIGITAL INFORMATION. HOWEVER IF THE REFUND IS REQUESTED ON THE BASIS OF TRANSACTION PROBLEMS OR SIMILAR TYPES OF ISSUES, DEPENDING UPON THE SITUATION REFUND MAY BE APPROVED BY THE COMPANY AND WILL BE EFFECTED WITHIN 7 DAYS OF GETTING PROPER REQUEST THROUGH THE CONPANY&rsquo;S E-MAIL ID FOR THE PURPOSE.<br />\n" +
                        "(E) AS OF NOW, THE REGISTERED USERS WHO MADE THE REQUIRED PAYMENT CAN USE ALL THE MATERIALS AVAILABLE IN THE APP AND THE APP WILL BE AD-FREE. TO IDENTIFY AND PROMOTE UNIQUE USERS OF THE APP AS WELL AS UNAUTHORISED APP USAGE, REGISTRATION IS DONE BASED ON THE UNIQUE DEVICE ID-EMAIL ID COMBINATION. IF YOU CHANGE THE DEVICE ON WHICH THE APP IS INSTALLED YOU WILL HAVE TO REQUEST THE COMPANY TO GET IT ACTIVATED ON THE NEW DEVICE. ACTIVATION OF THE APP ON THE NEW DEVICE IS THE SOLE DISCRETION OF THE COMPANY AND ACTIVATION WILL BE DONE FOR THE SAME EMAIL ID-NEW UNIQUE DEVICE ID COMBINATION. YOU WILL NOT GET THE APP ACTIVATED FOR A NEW EMAIL ID IN ANY CIRCUMSTANCES.&nbsp;<br />\n" +
                        "SUSPENSION OF SERVICE: The Company reserves the right to suspend or stop providing any services relating to the App, with or without notice, and shall have no liability or responsibility to you in any manner.</p>\n" +
                        "\n" +
                        "<p>IN-THE-APP ADVERTISEMENTS:</p>\n" +
                        "\n" +
                        "<p>We accept no responsibility for advertisements or the subsequent goods purchase or services in the advertisements contained within the App and the advertiser, not the Company, is responsible for any issues related to such goods and/or services.<br />\n" +
                        "&nbsp;DEVICES AND CONNECTIVITY: The provision of the Mobile Application does not include the provision of a mobile or device or other necessary equipment to access the App or the Services. To use the App or Services, you will require internet connectivity and appropriate devices. You acknowledge that the terms of agreement with your respective internet provider will continue to apply when using the App. As a result, you may be charged by the internet service provider for access to network connection services for the duration of the connection while accessing the App or any such third party charges as may arise. You accept responsibility for any such charges that arise.</p>\n" +
                        "\n" +
                        "<p>The above Terms constitute the entire agreement between you and the Company concerning your use of the App.<br />\n" +
                        "The Company reserves the right to append/update these Terms from time to time whenever necessary. In such cases, the updated Terms will be coming to effect immediately, and the current Terms will be available through the App. You are supposed to regularly review these Terms so that you will come to know changes to them and you will be bound by the new policy upon your continued use of the App. No other variation to these Terms shall be effective unless in writing and signed by an authorised representative on behalf of the Company.<br />\n" +
                        "These Terms shall be governed by and construed in accordance with Indian law and you agree to submit to the exclusive jurisdiction of the Indian Courts.<br />\n" +
                        "If any provision(s) of these Terms is held by a court of competent jurisdiction to be invalid or unenforceable, then such provision(s) shall be construed, as nearly as possible, to reflect the intentions of the parties (as reflected in the provision(s)) and all other provisions shall remain in full force and effect.<br />\n" +
                        "The Company&rsquo;s failure to exercise or enforce any right or provision of these Terms shall not constitute a waiver of such right or provision unless acknowledged and agreed to by the Company in writing.</p>\n "

        ));
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.advclose);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ad.dismiss();
            }
        });

        alertadd.setView(view);
       ad = alertadd.show();

    }

    private boolean isDouble(String str)
    {
        try
        {
            Double.parseDouble(str);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }

    private double getAmount()
    {
            amount = 10.0;
        if (isDouble(amt.getText().toString()))
        {
            amount = Double.parseDouble(amt.getText().toString());
            return amount;
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Paying Default Amount â‚¹10", Toast.LENGTH_LONG).show();
            return amount;
        }
    }

    public void makePayment(View view) {

//        String phone = UPhonenumber;
        String phone = "9946822286";
        String productName = "product_name";
//        String firstName = UName;
        String firstName = "SP LOGIC TECHNOLOGIES PRIVATE LIMITED";
        String txnId = "0nf7" + System.currentTimeMillis();

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());


//        String email = UEmail;
        String email = "logixsp@gmail.com";
        String sUrl = "https://test.payumoney.com/mobileapp/payumoney/success.php";
        String fUrl = "https://test.payumoney.com/mobileapp/payumoney/failure.php";
        String udf1 = "";
        String udf2 = "";
        String udf3 = "";
        String udf4 = "";
        String udf5 = "";
        boolean isDebug = false;

//................................Live credentails..................................................
        String key = "ykxo8c6u";
        String merchantId = "5731155";
//...................................Live credentails ends..........................................
//.............................testing key and merchand id...........................................

     /*  String key = "dRQuiA";
       String merchantId = "4928174" ;*/

//.............................testing key and merchand id  ends ...........................................


        //calling webservice INSERT TRANSACTION DETAILS
        //this service is used to store the timedata and device id of the user when transaction is started
        InsertPaymentTrans insertPaymentTrans = new InsertPaymentTrans();
        insertPaymentTrans.addProperties(UuserID, UAndroidID, formattedDate);
        insertPaymentTrans.execute();


        PayUmoneySdkInitilizer.PaymentParam.Builder builder = new PayUmoneySdkInitilizer.PaymentParam.Builder();
        builder.setAmount(getAmount())
                .setTnxId(txnId)
                .setPhone(phone)
                .setProductName(productName)
                .setFirstName(firstName)
                .setEmail(email)
                .setsUrl(sUrl)
                .setfUrl(fUrl)
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setIsDebug(isDebug)
                .setKey(key)
                .setMerchantId(merchantId);

        PayUmoneySdkInitilizer.PaymentParam paymentParam = builder.build();

//             server side call required to calculate hash with the help of <salt>
//             <salt> is already shared along with merchant <key>
     /*        serverCalculatedHash =sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|<salt>)

             (e.g.)

             sha512(FCstqb|0nf7|10.0|product_name|piyush|piyush.jain@payu.in||||||MBgjYaFG)

             9f1ce50ba8995e970a23c33e665a990e648df8de3baf64a33e19815acd402275617a16041e421cfa10b7532369f5f12725c7fcf69e8d10da64c59087008590fc
*/

        // Recommended
        calculateServerSideHashAndInitiatePayment(paymentParam);

//        testing purpose

       /* String salt = "";
        String serverCalculatedHash=hashCal(key+"|"+txnId+"|"+getAmount()+"|"+productName+"|"
                +firstName+"|"+email+"|"+udf1+"|"+udf2+"|"+udf3+"|"+udf4+"|"+udf5+"|"+salt);

        paymentParam.setMerchantHash(serverCalculatedHash);

        PayUmoneySdkInitilizer.startPaymentActivityForResult(MyActivity.this, paymentParam);*/
    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }

    private void calculateServerSideHashAndInitiatePayment(final PayUmoneySdkInitilizer.PaymentParam paymentParam) {


      //  String url = "https://test.payumoney.com/payment/op/calculateHashForTest";
        String url = "https://pcplsoftwares.biz/payumoney/moneyhash.php";

        Toast.makeText(this, "Please wait... Generating hash from server ... ", Toast.LENGTH_LONG).show();

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.has(SdkConstants.STATUS)) {
                        String status = jsonObject.optString(SdkConstants.STATUS);
                        if (status != null || status.equals("1"))
                        {
                            String hash = jsonObject.getString(SdkConstants.RESULT);
                            Log.i("app_activity", "Server calculated Hash :  " + hash);

                            paymentParam.setMerchantHash(hash);

                            PayUmoneySdkInitilizer.startPaymentActivityForResult(MyActivity.this, paymentParam);
                        } else {
                            Toast.makeText(MyActivity.this,
                                    jsonObject.getString(SdkConstants.RESULT),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                if (error instanceof NoConnectionError) {
                    Toast.makeText(MyActivity.this,
                            MyActivity.this.getString(R.string.connect_to_internet),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MyActivity.this,
                            error.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return paymentParam.getParams();
            }
        };
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PayUmoneySdkInitilizer.PAYU_SDK_PAYMENT_REQUEST_CODE) {


            /*if(data != null && data.hasExtra("result")){
              String responsePayUmoney = data.getStringExtra("result");
                if(SdkHelper.checkForValidString(responsePayUmoney))
                    showDialogMessage(responsePayUmoney);
            } else {
                showDialogMessage("Unable to get Status of Payment");
            }*/

            if (resultCode == RESULT_OK)
            {
                Log.i(TAG, "Success - Payment ID : " + data.getStringExtra(SdkConstants.PAYMENT_ID));

                Log.i(TAG,""+data.getStringArrayExtra(SdkConstants.USER_NAME));

                Log.i("Jacky",""+data.getStringArrayExtra(SdkConstants.AMOUNT));

                    paymentId = data.getStringExtra(SdkConstants.PAYMENT_ID);

               // showDialogMessage("Payment Success Id : " + paymentId);


                // webservice updating the server side that paymnet susccessfully completed
                UpdatePaymentTrans updatePaymentTrans = new UpdatePaymentTrans();
                int transID = sharedPreferences_loggedUser.getInt("TransID_primaryKey", 0);
                updatePaymentTrans.addPropertyforupdate(paymentId, 1, 0, transID);
                updatePaymentTrans.execute();


            } else if (resultCode == RESULT_CANCELED) {
                Log.i(TAG, "failure");
                showDialogMessage("cancelled");
            } else if (resultCode == PayUmoneySdkInitilizer.RESULT_FAILED) {
                Log.i("app_activity", "failure");

                if (data != null) {
                    if (data.getStringExtra(SdkConstants.RESULT).equals("cancel")) {

                    } else {
                        showDialogMessage("failure");
                    }
                }
                //Write your code if there's no result
            } else if (resultCode == PayUmoneySdkInitilizer.RESULT_BACK) {
                Log.i(TAG, "User returned without login");
                showDialogMessage("User returned without login");
            }
        }
    }

    private void showDialogMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(TAG);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }


    public void setTranscationID(String response) {

        try {
            int transID = Integer.parseInt(response);
            SharedPreferences.Editor editor = sharedPreferences_loggedUser.edit();
            editor.putInt("TransID_primaryKey", transID);
            editor.commit();

           }
        catch (Exception expt) {
            Toast.makeText(getApplicationContext(), "Error Occured", Toast.LENGTH_SHORT).show();
        }
    }

    public void SuccessfullyPaid(String response)
    {

        if (response.equals("True"))
        {
            sharedPreferences_loggedUser = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences_loggedUser.edit();
            editor.putBoolean("paidStatus", true);
            editor.commit();
            Log.d("paidstatus", "working");

            UpdateUserPaymentStatus status = new UpdateUserPaymentStatus();
            status.set_UserID(UuserID);
            status.execute();


        }
    }

    public void completed_trans(String response)
    {
        Bundle bundle = new Bundle();
        bundle.putString("transId",paymentId);
        bundle.putDouble("amount",amount);
        Intent intent = new Intent(MyActivity.this,TransactionSucess.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }


}