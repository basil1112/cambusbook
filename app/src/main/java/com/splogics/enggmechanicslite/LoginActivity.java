package com.splogics.enggmechanicslite;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.splogics.enggmechanicslite.WebService.CheckUserwithAndroidID;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private EditText txtEmail;
    private Button btnLogin;
    private LinearLayout signup_layout,login_layout;
    private CardView logincardview;
    private TextView login_guest;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String email;
    public static LoginActivity objActivity;
    private ProgressBar progressBar;

    public static LoginActivity getObject() {
        return objActivity;
    }
    SharedPreferences sh;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        objActivity = this;

        sh = getSharedPreferences("MyLogDetails", MODE_PRIVATE);

        progressBar = (ProgressBar) findViewById(R.id.login_progress);
        logincardview = (CardView) findViewById(R.id.logincardview);
        signup_layout = (LinearLayout) findViewById(R.id.linear_signup) ;
        signup_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(intent);
                finish();


            }
        });

        login_guest = (TextView) findViewById(R.id.guest_login);
        login_guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                if(isConnected)
                {
                    //Guest login
                    sh = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                    SharedPreferences.Editor ed = sh.edit();
                    ed.putBoolean("paidStatus", false);
                    ed.putInt("guestStatus", 1);
                    ed.putString("LoggedUser_Name", "Guest");
                    ed.commit();
                    login_guest.setEnabled(false);
                    progressBar.setVisibility(View.VISIBLE);

                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"You are not connected to internet, please enable connectivity and try again",Toast.LENGTH_LONG).show();
                }

            }
        });

        String Oldemail = sh.getString("LoggedUser_Email","");
        if(!Oldemail.equals(""))
        {
            email = Oldemail;
            //already paid ,so we have to check login credential
            //has to go through email checking and phone checking

            ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            if(isConnected)
            {
                CheckUserwithAndroidID log_email = new CheckUserwithAndroidID();
                log_email.LoginEmail(email);
                log_email.execute();
                progressBar.setVisibility(View.VISIBLE);
                logincardview.setVisibility(View.GONE);
            }
            else
            {
                Toast.makeText(getApplicationContext(),"You are not connected to internet, please enable connectivity and try again",Toast.LENGTH_LONG).show();
            }

        }
        else
        {
            //not a paid user

        txtEmail = (EditText) findViewById(R.id._login_input_email);
        btnLogin = (Button) findViewById(R.id.login_btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                email = txtEmail.getText().toString();

                if (email.equals("")) {
                    txtEmail.setError("Email cannot be Blank");
                }
                else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    txtEmail.setError("Enter valid email format");
                }
                else
                {
                    ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                    if(isConnected) {
                        btnLogin.setEnabled(false);
                        //call webservice for login
                        CheckUserwithAndroidID log_email = new CheckUserwithAndroidID();
                        log_email.LoginEmail(email);
                        log_email.execute();
                        progressBar.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"You are not connected to internet, please enable connectivity and try again",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        }

    }

    public void loginRespose(String resposne) {

       String _myAndroidDeviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        try
        {
            JSONArray jsonArray = new JSONArray(resposne);
            int size = jsonArray.length();
            if(size > 0)
            {
                for(int i = 0; i < size ; i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String androidID = jsonObject.getString("AndroidID");
                    if(androidID.equals(_myAndroidDeviceId))
                    {
                        sh = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                        SharedPreferences.Editor ed = sh.edit();
                        ed.putBoolean("Logged", true);
                        ed.putString("LoggedUser_userID",""+jsonObject.getString("PK_User_ID"));
                        ed.putString("LoggedUser_Email", email);
                        ed.putString("LoggedUser_Name",jsonObject.getString("FullName"));
                        ed.putString("LoggedUser_Phone",jsonObject.getString("Phone"));
                        ed.putString("LoggedUser_AndroidID",jsonObject.getString("AndroidID"));
                        if(jsonObject.getString("PaymentStatus").equals("1"))
                        {
                            ed.putBoolean("paidStatus",true);

                        }
                        else
                        {
                            ed.putBoolean("paidStatus",false);
                        }
                            ed.putInt("guestStatus",0);
                        ed.commit();

                        Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
                        startActivity(intent);
                        finish();

                    }
                    else
                    {
                        sh = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                        SharedPreferences.Editor ed = sh.edit();
                        ed.putBoolean("Logged", true);
                        ed.putString("LoggedUser_userID",""+jsonObject.getString("PK_User_ID"));
                        ed.putString("LoggedUser_Email", email);
                        ed.putString("LoggedUser_Name",jsonObject.getString("FullName"));
                        ed.putString("LoggedUser_Phone",jsonObject.getString("Phone"));
                        ed.putString("LoggedUser_AndroidID",jsonObject.getString("AndroidID"));
                        ed.putBoolean("paidStatus",false);
                        ed.putInt("guestStatus",0);
                        ed.commit();
                        Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }

            else
            {
                //no user exists

                Toast.makeText(LoginActivity.this,"Invalid Username OR Password",Toast.LENGTH_SHORT).show();

                startActivity(new Intent(LoginActivity.this,LoginActivity.class));

            }

        }
        catch (Exception ex)
        {

        }



       /* if (resposne.equals(_myAndroidDeviceId))
        {
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(intent);
        }
        else
        {

            Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
            startActivity(intent);
        }*/

    }
}
