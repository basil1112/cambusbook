package com.splogics.enggmechanicslite.WebService;

import android.os.AsyncTask;

import com.splogics.enggmechanicslite.MyActivity;


public class InsertPaymentTrans extends AsyncTask<Void,Void,String> {

    private String userID,androidID,DateTime,transNo;

    public void addProperties(String userId,String androidId,String datetime)
    {

        this.userID=userId;
        this.androidID=androidId;
        this.DateTime=datetime;
      //  this.transNo = transno;

    }

    @Override
    protected String doInBackground(Void... params) {

        WebServiceCaller caller = new WebServiceCaller();
        caller.setSoapActon("http://tempuri.org/InsertPaymentTrans");
        caller.setSoapObject("http://tempuri.org/","InsertPaymentTrans");
        caller.addProperty("UserID",userID);
        caller.addProperty("AndroidID",androidID);
        caller.addProperty("Date",DateTime);
        caller.addProperty("TransactionNo","");
        caller.addProperty("TransactionStatus",0);
        caller.addProperty("ErrorCode",0);
        caller.callWebService();

        return caller.getResponse();

    }

    @Override
    protected void onPostExecute(String s) {

        MyActivity.getObject().setTranscationID(s);

    }
}
