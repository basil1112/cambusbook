package com.splogics.enggmechanicslite.WebService;

import android.os.AsyncTask;

import com.splogics.enggmechanicslite.Fragments.FragmentMain;

/**
 * Created by sanoop on 27-12-16.
 */

public class GetBookDetails extends AsyncTask<Void, Void, String> {

    private int categoryID;
    private String subjectID;

    public void addSubjectID_and_CategoryID(int catID, String subID) {

        this.categoryID = catID;
        this.subjectID = subID;

    }

    @Override
    protected String doInBackground(Void... params) {
        WebServiceCaller caller = new WebServiceCaller();
        caller.setSoapObject("http://tempuri.org/", "GetBookDetails");
        caller.setSoapActon("http://tempuri.org/GetBookDetails");
        caller.addProperty("BookID", "");
        caller.addProperty("SubjectID",subjectID);
        caller.callWebService();
        return caller.getResponse();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        FragmentMain.getObject().setBooks_toView(s);


    }
}
