package com.splogics.enggmechanicslite.WebService;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


final public class WebServiceCaller {

    private String response;
    private int responseCode;
    private SoapObject request;
    private String url;
    private String exception;
    private String soapAction;

    public void setSoapObject(String nameSpace, String methodName) {
        request = new SoapObject(nameSpace, methodName);
     // url = "http://192.168.1.202:8012/WebServices/BookAppService.asmx";
      //  url="http://test.pcplglobal.com/WebServices/BookAppService.asmx";
        url="http://book.splogics.com/WebServicesNew/BookAppService.asmx";
    }

    public void addProperty(String key, Object value) {

        request.addProperty(key, value);

    }

    public void setSoapActon(String soapActon) {

        this.soapAction = soapActon;

    }

    public void setUrl(String url) {

        this.url = url;

    }

    public String getResponse() {

        return response;

    }

    public void callWebService() {

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;

        envelope.setOutputSoapObject(request);

        HttpTransportSE httpTransportSE = new HttpTransportSE(url, 60 * 1000);

        try {

            httpTransportSE.call(soapAction, envelope);

            response = ((SoapPrimitive) envelope.getResponse()).toString();

        } catch (Exception ex) {

            response = ex.toString();

        }

    }


}
