package com.splogics.enggmechanicslite.WebService;

import android.os.AsyncTask;

import com.splogics.enggmechanicslite.Fragments.ChapterFragment;

public class GetAllChapters extends AsyncTask<Void, Void, String> {

    private String mbookID = "";

    public void addBookID(String bookID) {
        this.mbookID = bookID;
    }

    @Override
    protected String doInBackground(Void... params) {
        WebServiceCaller caller = new WebServiceCaller();
        caller.setSoapObject("http://tempuri.org/", "GetAllChaptersOfBook");
        caller.setSoapActon("http://tempuri.org/GetAllChaptersOfBook");
        caller.addProperty("BookID", mbookID);
        caller.callWebService();
        return caller.getResponse();
    }

    @Override
    protected void onPostExecute(String s) {
        //super.onPostExecute(s);
        ChapterFragment.getObject().setChapterBasedOnBookID(s);
    }
}
