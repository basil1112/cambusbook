package com.splogics.enggmechanicslite.WebService;

import android.os.AsyncTask;
import android.util.Log;

import com.splogics.enggmechanicslite.Fragments.FirstPageFragment;


public class GetAllCategories extends AsyncTask<Void, Void, String> {
    @Override
    protected String doInBackground(Void... params) {

        WebServiceCaller caller = new WebServiceCaller();
        caller.setSoapObject("http://tempuri.org/", "GetAllCategories");
        caller.setSoapActon("http://tempuri.org/GetAllCategories");
        caller.addProperty("CategoryID","");
        caller.callWebService();
        return caller.getResponse();
    }


    @Override
    protected void onPostExecute(String s) {
      //  super.onPostExecute(s);
        Log.d("happy",s);
     // FragmentMain.getObject().setBooks_toView(s);
        FirstPageFragment.getObject().setCategory_fromweb(s);

    }
}


