package com.splogics.enggmechanicslite.WebService;

import android.os.AsyncTask;

import com.splogics.enggmechanicslite.LoginActivity;


public class CheckUserwithAndroidID extends AsyncTask<Void,Void,String> {

  private String Email;
    public void LoginEmail(String email)
    {
        this.Email = email;
    }

    @Override
    protected String doInBackground(Void... params) {

        WebServiceCaller caller = new WebServiceCaller();
        caller.setSoapObject("http://tempuri.org/","CheckUserwithAndroidID");
        caller.setSoapActon("http://tempuri.org/CheckUserwithAndroidID");
        caller.addProperty("EmailID",Email);
        caller.callWebService();
        return caller.getResponse();
    }

    @Override
    protected void onPostExecute(String s) {

        LoginActivity.getObject().loginRespose(s);

    }
}
