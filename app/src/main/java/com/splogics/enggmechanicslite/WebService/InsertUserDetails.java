package com.splogics.enggmechanicslite.WebService;

import android.os.AsyncTask;
import android.util.Log;

import com.splogics.enggmechanicslite.SignUpActivity;

/**
 * Created by sanoop on 11-01-17.
 */

public class InsertUserDetails extends AsyncTask<Void,Void,String>{

    private String mFullName="",mEmail="",mMobNo="",mAndroidID="";

    public void addUserDetails(String name,String email,String mobno,String androidID)
    {
        this.mFullName = name;
        this.mEmail = email;
        this.mMobNo = mobno;
        this.mAndroidID = androidID;
    }

    @Override
    protected String doInBackground(Void... params) {

        WebServiceCaller caller = new WebServiceCaller();
        try {

            caller.setSoapObject("http://tempuri.org/", "InsertUserDetails");
            caller.setSoapActon("http://tempuri.org/InsertUserDetails");
            caller.addProperty("FullName", mFullName);
            caller.addProperty("PaymentStatus","");
            caller.addProperty("Email", mEmail);
            caller.addProperty("Phone", mMobNo);
            caller.addProperty("AndroidID", mAndroidID);
            caller.callWebService();
        }
        catch(Exception exp)
        {
            Log.d("edanari",""+exp);
        }
        return caller.getResponse();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        Log.d("SignupREs",""+s);
        SignUpActivity.getObject().register_success(s);

    }
}
