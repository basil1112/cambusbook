package com.splogics.enggmechanicslite.WebService;

import android.os.AsyncTask;
import android.util.Log;

import com.splogics.enggmechanicslite.MyActivity;

public class UpdateUserPaymentStatus extends AsyncTask<Void,Void,String> {

    private String uid;
    public void set_UserID(String uid)
    {

        this.uid = uid;

    }

    @Override
    protected String doInBackground(Void... params) {

        Log.d("notmyError",uid);
        WebServiceCaller caller = new WebServiceCaller();
        caller.setSoapObject("http://tempuri.org/", "UpdateUserPaymentStatus");
        caller.setSoapActon("http://tempuri.org/UpdateUserPaymentStatus");
        caller.addProperty("UserID",uid);
        caller.addProperty("PaymentStatus","1");
        caller.callWebService();
        return caller.getResponse();
    }

    @Override
    protected void onPostExecute(String s) {

        MyActivity.getObject().completed_trans(s);

    }
}
