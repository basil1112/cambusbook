package com.splogics.enggmechanicslite.WebService;

import android.os.AsyncTask;

import com.splogics.enggmechanicslite.SignUpActivity;

/**
 * Created by sanoop on 25-01-17.
 */

public class CheckUserExistance extends AsyncTask<Void,Void,String> {

    String Email;
    public void checkingUsername(String email)
    {
        this.Email = email;
    }
    @Override
    protected String doInBackground(Void... params) {

        WebServiceCaller caller = new WebServiceCaller();
        caller.setSoapActon("http://tempuri.org/CheckUserExistance");
        caller.setSoapObject("http://tempuri.org/","CheckUserExistance");
        caller.addProperty("EmailID",Email);
        caller.callWebService();
        return caller.getResponse();

    }

    @Override
    protected void onPostExecute(String s) {

        SignUpActivity.getObject().user_exists(s);



    }
}
