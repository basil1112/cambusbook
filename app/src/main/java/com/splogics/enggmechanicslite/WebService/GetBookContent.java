package com.splogics.enggmechanicslite.WebService;

import android.os.AsyncTask;

import com.splogics.enggmechanicslite.Fragments.DataViewPage;

/**
 * Created by sanoop on 05-01-17.
 */

public class GetBookContent extends AsyncTask<Void,Void,String> {

    private int mbookID;
    private int mchapterID;
    public void addBookChapterID(int bookID,int chapterId)
    {
        mbookID = bookID;
        mchapterID = chapterId;
    }


    @Override
    protected String doInBackground(Void... params) {

        WebServiceCaller caller = new WebServiceCaller();
        caller.setSoapObject("http://tempuri.org/", "GetBookContent");
        caller.setSoapActon("http://tempuri.org/GetBookContent");
        caller.addProperty("BookID",mbookID);
        caller.addProperty("ChapterID",mchapterID);
        caller.callWebService();
        return caller.getResponse();

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        DataViewPage.getObject().setBookContent(s);
    }
}
