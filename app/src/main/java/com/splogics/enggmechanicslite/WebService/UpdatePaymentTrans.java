package com.splogics.enggmechanicslite.WebService;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.splogics.enggmechanicslite.HomeActivity;
import com.splogics.enggmechanicslite.MyActivity;


public class UpdatePaymentTrans extends AsyncTask<Void, Void, String> {

    private String transationNumber;
    private int TransactionStatus, eroorCode, transPrimaryKey;


    public void addPropertyforupdate(String transNumber, int tranStatus, int eroorCode, int transID) {

        this.transationNumber = transNumber;
        this.TransactionStatus = tranStatus;
        this.eroorCode = eroorCode;
        this.transPrimaryKey = transID;

    }

    @Override
    protected String doInBackground(Void... params) {

        Log.i("testingbasil",""+transationNumber+"..."+TransactionStatus+"...."+eroorCode+"...."+transPrimaryKey);
        WebServiceCaller caller = new WebServiceCaller();
        caller.setSoapObject("http://tempuri.org/", "UpdatePaymentTrans");
        caller.setSoapActon("http://tempuri.org/UpdatePaymentTrans");
        caller.addProperty("TransactionID",transPrimaryKey);
        caller.addProperty("TransactionNumber",transationNumber);
        caller.addProperty("TransactionStatus",TransactionStatus);
        caller.addProperty("ErrorCode",eroorCode);
        caller.callWebService();
        return caller.getResponse();
       //return null;

    }

    @Override
    protected void onPostExecute(String s) {

        Log.i("PaidMan",""+s);
        MyActivity.getObject().SuccessfullyPaid(s);

    }
}
