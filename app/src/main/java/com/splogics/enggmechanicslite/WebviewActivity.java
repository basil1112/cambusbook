package com.splogics.enggmechanicslite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebviewActivity extends AppCompatActivity {

    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        mWebView = (WebView) findViewById(R.id.webview_container);

        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        String url="<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<a href=\"http://www.w3schools.com\">This is a link</a>\n" +
                "<img src=\"http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/sign-check-icon.png\" width=\"100px\" height=\"100px\"/>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n";
//        mWebView.loadUrl(url);

//        mWebView.loadData(data,"text/html", null);
        mWebView.loadDataWithBaseURL(null,url, "text/html", "UTF-8", null);
        mWebView.loadUrl(url);
    }
}
