package com.splogics.enggmechanicslite;


import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;


public class SplashActivity extends AppCompatActivity {

    private static int splashlength = 1500;
    private TextView txtTitlename;
    private Typeface font1;
    Boolean alreadySignup;

    SharedPreferences sh;

    private static final int READ_CONTACTS_PERMISSIONS_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        txtTitlename = (TextView) findViewById(R.id.splash_titlename);

        //testing the shared preferene for the toggle og login and sign up
        sh = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
        alreadySignup = sh.getBoolean("Logged", false);

        if (Build.VERSION.SDK_INT > 23) {
            //higher versions
        if((Build.VERSION.SDK_INT == 25) || (Build.VERSION.SDK_INT == 24) )
        {


            if (alreadySignup) {
                //to Home activity
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, splashlength);


            } else {
                // to sign up for new user   one time registeration
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, splashlength);

            }


        }
            if(Build.VERSION.SDK_INT == 45) {
                Log.d("Higher", "Working");

                try {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                            != PackageManager.PERMISSION_GRANTED) {

                        if (shouldShowRequestPermissionRationale(
                                Manifest.permission.INTERNET)) {
                            // Show our own UI to explain to the user why we need to read the contacts
                            // before actually requesting the permission and showing the default UI
                        }

                        requestPermissions(new String[]{Manifest.permission.INTERNET},
                                READ_CONTACTS_PERMISSIONS_REQUEST);
                    }
                } catch (Exception exp) {
                    Log.d("Higher", "" + exp);
                }
            }
        } else {
            //lower versions
            //all permission are automatically granted

            Log.d("Higher","Low version");

            if (alreadySignup) {
                //to Home activity
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, splashlength);

            } else {
                // to sign up for new user   one time registeration
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, splashlength);

            }

        }


//        String myAndroidDeviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == READ_CONTACTS_PERMISSIONS_REQUEST) {

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("Permission", "granted");

                if (alreadySignup) {
                    //to Home activity
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, splashlength);

                } else {
                    // to sign up for new user   one time registeration
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, splashlength);

                }

            } else {
                Log.d("Permission", "not accepeted");
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
