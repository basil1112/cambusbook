package com.splogics.enggmechanicslite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.view.View;
import android.widget.AdapterView;

import com.splogics.enggmechanicslite.Adapter.BooksAdapter;
import com.splogics.enggmechanicslite.WebService.GetAllCategories;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{

//   ACTAULLY STARTED WITH MAIN ACTIVITY BUT CHANGES TO FRAGMENT MAIN ACTIVITY IS NOT USED NOW :)

    private GridView myRecyclerView;
    ArrayList<Integer> mbookImages;
    ArrayList<String> mbookTitle;

    private static MainActivity myBook_fromWeb;

    public static MainActivity getObject() {
        return myBook_fromWeb;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mbookImages = new ArrayList<>();

       /*
        mbookImages.add(R.mipmap.pic1);
        mbookImages.add(R.mipmap.pic2);
        mbookImages.add(R.mipmap.pic3);
        mbookImages.add(R.mipmap.pic4);
        mbookImages.add(R.mipmap.pic5);
        mbookImages.add(R.mipmap.pic6);
        mbookImages.add(R.mipmap.pic7);
        mbookImages.add(R.mipmap.pic8);
        mbookImages.add(R.mipmap.pic9);
        mbookImages.add(R.mipmap.pic10);
        mbookImages.add(R.mipmap.pic1);
        mbookImages.add(R.mipmap.pic2);
        mbookImages.add(R.mipmap.pic3);
        mbookImages.add(R.mipmap.pic4);

        */

        mbookTitle = new ArrayList<>();

        GetAllCategories loadingCategories = new GetAllCategories();
        loadingCategories.execute();


        Log.d("Working", "Its awesome");


        myRecyclerView = (GridView) findViewById(R.id.recycler_books);
        BooksAdapter adapter = new BooksAdapter(this, mbookImages, mbookTitle,null,null);
        myRecyclerView.setAdapter(adapter);
        myRecyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

//        Intent intent = new Intent(MainActivity.this,WebviewActivity.class);
//        startActivity(intent);

    }
    public void setBooks_toView(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            int size = jsonArray.length();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    mbookTitle.add(jsonObject.getString("Category"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
