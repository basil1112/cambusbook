package com.splogics.enggmechanicslite.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.splogics.enggmechanicslite.MyActivity;
import com.splogics.enggmechanicslite.R;
import com.splogics.enggmechanicslite.SignUpActivity;

import static android.content.Context.MODE_PRIVATE;

public class html_findReplace extends Fragment {

    private LayoutInflater mInflater;
    TextView textView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mInflater = inflater;
        View rootView = inflater.inflate(R.layout.payment_layout, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        SharedPreferences sharedPreferences_loggedUser = getContext().getSharedPreferences("MyLogDetails", MODE_PRIVATE);
        int guestStatus = sharedPreferences_loggedUser.getInt("guestStatus", 0);
        if (guestStatus == 1) {
            Bundle b = new Bundle();
            b.putInt("temp", 1);
            Intent intent = new Intent(getActivity(), SignUpActivity.class);
            intent.putExtras(b);
            getActivity().startActivity(intent);

        } else {

            textView = (TextView) view.findViewById(R.id.textpaynow);
            blink(view);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(getContext(), MyActivity.class);
                    startActivity(intent);
                }
            }, 2000);

        }
    }

    public void blink(View view) {

        Animation animation1 = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
        textView.startAnimation(animation1);
    }
}
