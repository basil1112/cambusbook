package com.splogics.enggmechanicslite.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.splogics.enggmechanicslite.R;


public class OtherApps extends Fragment {


    private WebView mOtherApp_webview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_other_apps, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mOtherApp_webview = (WebView) view.findViewById(R.id.web_otherapps);
        mOtherApp_webview.loadUrl("https://play.google.com/store/apps/developer?id=SP%20Logic%20Technologies%20Pvt%20Ltd&hl=en");
        super.onViewCreated(view, savedInstanceState);
    }
}
