package com.splogics.enggmechanicslite.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.splogics.enggmechanicslite.R;


public class ContactFragment extends Fragment {

    private Button btn_send;
    private TextView txtname,txtemail,txtcompany,txthelpnote;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        txtname = (TextView) view.findViewById(R.id.contact_editText_name);
        txtemail = (TextView) view.findViewById(R.id.contact_editText_email);
        txtcompany = (TextView) view.findViewById(R.id.contact_editText_company);
        txthelpnote = (TextView) view.findViewById(R.id.contact_editText_helpnote);

        btn_send = (Button) view.findViewById(R.id.contact_btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String _email = txtemail.getText().toString();

                if(txtname.getText().toString().equals(""))
                {
                    txtname.setError("Name cannot be blank");
                }
                else if(txtemail.getText().toString().equals(""))
                {

                    txtemail.setError("Email cannot be blank");
                }
                else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(_email).matches()) {
                {
                    txtemail.setError("Enter valid email format");
                }
                }
                else if(txthelpnote.getText().toString().equals(""))
                {
                    txthelpnote.setError("Please Enter your note");
                }
                else
                {
                    String msg = txthelpnote.getText().toString() +" CONTACT BACK : " +txtemail.getText().toString();
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{"logixsp@gmail.com"});
                    i.putExtra(Intent.EXTRA_SUBJECT,txtname.getText().toString() );
                    i.putExtra(Intent.EXTRA_TEXT, msg);
                    try {
                        startActivity(Intent.createChooser(i, "Send mail using..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
}
