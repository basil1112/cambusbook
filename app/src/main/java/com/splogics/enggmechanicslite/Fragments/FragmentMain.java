package com.splogics.enggmechanicslite.Fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.splogics.enggmechanicslite.Adapter.BooksAdapter;
import com.splogics.enggmechanicslite.MyActivity;
import com.splogics.enggmechanicslite.R;
import com.splogics.enggmechanicslite.SignUpActivity;
import com.splogics.enggmechanicslite.WebService.GetBookDetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentMain extends Fragment {

    private LayoutInflater mInflater;
    private GridView myRecyclerView;
    private ProgressBar mProgressBar;
    public static FragmentMain myBookObject;

    public static FragmentMain getObject() {
        return myBookObject;
    }

    ArrayList<String> mbookImagesPath;
    ArrayList<Integer> mbookPaymentStatus;
    ArrayList<Integer> mbookImages;
    ArrayList<String> mbookTitle;
    ArrayList<Integer> mbookID;
    SharedPreferences msharedPreferences;
    AlertDialog ad;
    BooksAdapter adapter;
    Bundle bundle;
    int selected_category;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mInflater = inflater;
        View rootView = inflater.inflate(R.layout.activity_fragment_main, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        bundle = getArguments();
        if (bundle != null) {
            selected_category = bundle.getInt("book_categoryID");
        }
        mbookImages = new ArrayList<>();
        mbookID = new ArrayList<>();
        mbookTitle = new ArrayList<>();
        mbookImagesPath = new ArrayList<>();
        mbookPaymentStatus = new ArrayList<>();

        myBookObject = this;
        GetBookDetails bookDetails = new GetBookDetails();
        bookDetails.addSubjectID_and_CategoryID(selected_category,getContext().getString(R.string.subjectID));
        bookDetails.execute();

       /* mbookImages.add(R.mipmap.pic1);
        mbookImages.add(R.mipmap.pic2);
        mbookImages.add(R.mipmap.pic3);
        mbookImages.add(R.mipmap.pic4);
        mbookImages.add(R.mipmap.pic5);
        mbookImages.add(R.mipmap.pic6);
        mbookImages.add(R.mipmap.pic7);
        mbookImages.add(R.mipmap.pic8);
        mbookImages.add(R.mipmap.pic9);
        mbookImages.add(R.mipmap.pic10);
        mbookImages.add(R.mipmap.pic1);
        mbookImages.add(R.mipmap.pic2);
        mbookImages.add(R.mipmap.pic3);
        mbookImages.add(R.mipmap.pic4);*/

//        Log.d("Working", "Its awesome");


        mProgressBar = (ProgressBar) view.findViewById(R.id.fragmentmain_progressbar);
        myRecyclerView = (GridView) view.findViewById(R.id.recycler_books);
        adapter = new BooksAdapter(getContext(), mbookImages, mbookTitle, mbookImagesPath, mbookPaymentStatus);
        myRecyclerView.setAdapter(adapter);

        myRecyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pop_up(position);
            }
        });

    }

    public void pop_up(final int posi) {

        try {
            if (mbookPaymentStatus.get(posi) != 1) {
                // avoid pop for opening book for premium books ,checking is here :)

                String str = mbookTitle.get(posi);
                AlertDialog.Builder alertadd = new AlertDialog.Builder(getContext());
                LayoutInflater factory = LayoutInflater.from(getContext());
                final View view = factory.inflate(R.layout.confirmlayout, null);
                TextView textViewshopname = (TextView) view.findViewById(R.id.confirm_textview);

                textViewshopname.setText("Do you want to open " + str);
                Button btnYes = (Button) view.findViewById(R.id.confirm_btnYes);
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ad.dismiss();
                        //this is method which moves the fragment to order fragment carying the shop names/id
                        movetonext_fragment(posi);
                    }
                });
                Button btnNo = (Button) view.findViewById(R.id.confirm_btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ad.dismiss();
                    }
                });

                alertadd.setView(view);
                //ad = alertadd.show();

                movetonext_fragment(posi);

            } //premium checking ends
            else {

                SharedPreferences sharedPreferences_loggedUser = getContext().getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                Boolean paid_OR_not = sharedPreferences_loggedUser.getBoolean("paidStatus",false);
                if (paid_OR_not)
                {

                    String str = mbookTitle.get(posi);
                    AlertDialog.Builder alertadd = new AlertDialog.Builder(getContext());
                    LayoutInflater factory = LayoutInflater.from(getContext());
                    final View view = factory.inflate(R.layout.confirmlayout, null);
                    TextView textViewshopname = (TextView) view.findViewById(R.id.confirm_textview);

                    textViewshopname.setText("Do you want to open " + str);
                    Button btnYes = (Button) view.findViewById(R.id.confirm_btnYes);
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            ad.dismiss();
                            //this is method which moves the fragment to order fragment carying the shop names/id
                            movetonext_fragment(posi);
                        }
                    });
                    Button btnNo = (Button) view.findViewById(R.id.confirm_btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ad.dismiss();
                        }
                    });

                    alertadd.setView(view);
                  //  ad = alertadd.show();
                    movetonext_fragment(posi);

                }
                else {
                paymentpop();}

            }
        } catch (Exception exeArray) {
            Log.d("FragmentMain@exe", "" + exeArray);
        }
    }

    public void paymentpop()
    {
        try {
            AlertDialog.Builder alertadd = new AlertDialog.Builder(getContext());
            LayoutInflater factory = LayoutInflater.from(getContext());
            final View view = factory.inflate(R.layout.confirmlayout, null);
            TextView textViewshopname = (TextView) view.findViewById(R.id.confirm_textview);

            textViewshopname.setText("Premium content only premium users can view. Upgrade to pro ?");
            Button btnYes = (Button) view.findViewById(R.id.confirm_btnYes);
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferences_loggedUser = getContext().getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                    int guestStatus = sharedPreferences_loggedUser.getInt("guestStatus", 0);
                    if (guestStatus == 1)
                    {
                        Bundle b = new Bundle();
                        b.putInt("temp",1);
                        Intent intent = new Intent(getActivity(), SignUpActivity.class);
                        intent.putExtras(b);
                        getActivity().startActivity(intent);

                    }
                    else
                    {
                        Intent intent = new Intent(getActivity(), MyActivity.class);
                        getActivity().startActivity(intent);
                    }
                }
            });
            Button btnNo = (Button) view.findViewById(R.id.confirm_btnNo);
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ad.dismiss();
                }
            });

            alertadd.setView(view);
            ad = alertadd.show();

        } catch (Exception exeArray) {
            Log.d("First@exe", "" + exeArray);
        }

    }


    public void movetonext_fragment(int position) {

        Fragment myFragment = new ChapterFragment();
        FragmentManager manager = ((AppCompatActivity) getContext()).getSupportFragmentManager();

        Bundle b = new Bundle();
        b.putInt("BookID_toRead", mbookID.get(position));
        b.putString("BookTitle_toRead", mbookTitle.get(position));
        myFragment.setArguments(b);

//        msharedPreferences = ((AppCompatActivity) getContext()).getSharedPreferences("myShared_shopname", MODE_PRIVATE);
//        SharedPreferences.Editor editor = msharedPreferences.edit();
//        editor.putInt("Book ID", mbookID.get(position));
//        editor.apply();
//        Bundle bundle = new Bundle();
//        bundle.putInt("bookID", mbookID.get(position));
//        myFragment.setArguments(bundle);

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.content_frame, myFragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    public void setBooks_toView(String response) {

        try {
            JSONArray jsonArray = new JSONArray(response);
            int size = jsonArray.length();
            if (size > 0)
            {
                mbookID.clear();
                mbookTitle.clear();
                mbookImagesPath.clear();
                mbookPaymentStatus.clear();

                for (int i = size-1; i > -1; i--) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.getInt("PK_CategoryID") == selected_category)
                    {
                        mbookTitle.add(jsonObject.getString("BookName"));
                        mbookID.add(jsonObject.getInt("PK_BookID"));
                        mbookImagesPath.add(jsonObject.getString("CoverImage"));
                        mbookPaymentStatus.add(jsonObject.getInt("PaymentStatus"));
                    }
                }
                adapter.notifyDataSetChanged();
                mProgressBar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
