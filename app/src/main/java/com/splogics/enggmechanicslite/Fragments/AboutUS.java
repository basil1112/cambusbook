package com.splogics.enggmechanicslite.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splogics.enggmechanicslite.R;


public class AboutUS extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about_u, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {



//        TextView txtmatter = (TextView) view.findViewById(R.id.aboutusmatter);
//        txtmatter.setText(
//                "1.\tGeneral\n" +
//                "Engineering mechanics pro is an educational android application developed by SP Logic Technologies Pvt Ltd, Kerala, India. The subject engineering mechanics is a common subject in the starting year of any engineering course. It is included in almost all university syllabi to improve the logical reasoning and problem solving capacity of learners though the application will generally be to Civil, Mechanical and related streams. According to many of the students, engineering mechanics is a difficult subject. In this era of mobile and similar devices, we the SPLOGICS team tries to make learning of engineering mechanics easier. With this application, a learner will be able to study the subject engineering mechanics with fun and ease. Also, no need of carrying the textbooks that is you can learn engineering mechanics on the go! Most of the topics needed for different universities are included in this application. Practice problems are also included for ready reference by learners as well as teachers. We have taken maximum possible care to arrange the topics and tried to make the notes concise so that a slow learner will also can catch up with engineering mechanics. \n" +
//                "2.\tDisclaimer\n" +
//                "Though we have tried our level best to be accurate on the topics and problems, mistakes can occur in the application contents. No other option will be a substitute or shortcut for reading and learning a subject. Any student or faculty should read a number of standard textbooks for basic and advanced knowledge of a subject. Engineering mechanics is no exemption and you are encouraged to read many different books. This app is for facilitating the students for making the learning of engineering mechanics easier. You can use your textbooks and notebooks and also you are strongly advised to attend all lecture classes if possible which will definitely help you acquire knowledge. For any mistake or error in the content or topics or problems, the developers of this app are in anyway not responsible for any loss. You are also requested to read the terms and conditions of the usage of the apps carefully before downloading and using the app engineering mechanics.\n" +
//                "3.\tHow to use this app\n" +
//                "The team has put in maximum effort to make the application user friendly. After downloading and installing the engineering mechanics app for first time in your device, when you open the app from the launcher of the device, a login menu will first appear. From here, you can either “Sign Up” as a new user or you can try “Guest Login”. If you sign up, basic data about the user will be collected by the application which will later be used for payment. The contents in the app are arranged in two categories. For basic app users, only limited content will be freely available. To access the “Premium” content you have to register and pay the required amount. \n" +
//                "The topics are divided into “Statics” and “Dynamics” and also, questions, practice problems, previous university questions, solved question papers, etc., are made available. Due to time limitations, only limited number questions and practice problems are added at present. But as soon as possible we will add extra topics, question papers and practice problems without any additional cost. \n" +
//                "To access the topics you have to select a main topic either Statics or Dynamics. Then select the sub-topic under these two main headings. For example if you want to learn “Supports and Reactions”, you select “Statics”, and then select “Support and Reactions” section. Similarly you can easily access any topic if you are a premium user. The developers have the right to change any topic premium or free whenever they wish. \n" +
//                "If you are facing any problem while registering or payment, you can contact us at logixsp@gmail.com. Our representative will be assisting you to solve any issues related to registration or payment.\n" +
//                "4.\tContact us\n" +
//                "For feedbacks, queries, suggestions and any other matter you can contact us at em@splogics.com. We expect your valuable feedback through which only we can add more and more options in the app.\n");
//

    }
}
