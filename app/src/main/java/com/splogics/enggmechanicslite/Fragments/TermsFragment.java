package com.splogics.enggmechanicslite.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splogics.enggmechanicslite.R;


public class TermsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_terms, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
//        TextView textView = (TextView) view.findViewById(R.id.terms_matter);
//        textView.setText("Engineering Mechanics App - Terms and Conditions\n" +
//                "1.\tABOUT THE APPLICATION: A warm welcome to our mobile application - Engineering Mechanics (the “App”). This App is developed and maintained by SP Logic Technologies (P) Ltd. Kochi, Kerala, India (the “Company”). \n" +
//                "2.\tTHE APP PRIVACY POLICY: When you register with the Company and use the App, you will provide your name, email address, mobile number, transaction-related information, information you provide the Company when you contact the Company for help and credit card information for purchase. We collect device IDs for registration as well as for updates for better experience of the App in future. However, the Company assures the users that the data you provided as well as the data the App collected will not be used for any other purpose other than specified in the privacy policy of the App. We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorised access, disclosure, copying, use or modification.\n" +
//                "3.\tAGREEMENT: If you download or install the App on your device(s), you agree to the terms and conditions (the “Terms”) of the App and the privacy policy of the App. If you are not agreeing to the Terms or not satisfied with the privacy policy, you should immediately stop using it and should uninstall the App from your device(s). To help you with doubts and confusions, you can contact through our e-mail: logixsp@gmail.com. Any continued use of the App or services of the App means you agree to the Terms and satisfied by the privacy policy of the App.\n" +
//                "4.\tGENERAL CONDITIONS: The App is published and is available to you for your own personal use only. It should not be used for public or commercial purpose. It is strictly restricted to use the App for any illegal, fraudulent, unlawful or harmful purposes including (without limitation) copyright infringement. You are not supposed to use this app for any marketing purposes without the permission from the company. Also the user must comply with any applicable international laws, including the local laws in the user’s country.\n" +
//                "5.\tCONTENT: The copyright in all content materials in the App including all information, data, text, audio, photographs, graphics and videos and other material (the “Material“) is owned by or licensed to the Company and all rights of the mentioned are reserved. You can view the Material for your own personal use but you cannot otherwise copy, modify, change, re-produce, re-publish, display publicly, re-distribute, save, exchange or commercially exploit in any form whatsoever or use the Material without the Company’s explicit permission.\n" +
//                "6.\tTHE DOs and DONTs: You agree that when using the App you will strictly adhere to all the applicable laws and these Terms. In particular, but without limitation, you agree not to:\n" +
//                "a.\tIntentional attempt to get unauthorised access to the App or any networks, servers or computer systems directly or indirectly connected to the App.\n" +
//                "b.\tModify, adapt, translate or reverse engineer any part of the App or re-format or frame any portion of the pages displayed in the App, save to the extent expressly permitted by these Terms or by law.\n" +
//                "c.\tYou agree to indemnify the Company in full and on demand from and against any loss, damage, costs or expenses which they suffer or incur directly or indirectly as a result of your use of the App otherwise than in accordance with these Terms or applicable laws.\n" +
//                "7.\tTHE LOGOS: The logos (the “Logos“) used in the App are owned by the Company. You cannot use, copy, edit, vary, reproduce, publish, display, distribute, store, transmit, commercially exploit or disseminate the Logos without the prior written consent of the Company.\n" +
//                "8.\tLIMITATIONS OF LIABILITY: THE APP IS PROVIDED ON AN “AS IS” BASIS. USE OF THE APP IS AT YOUR OWN RISK. TO THE MAXIMUM EXTENT PERMITTED BY LAW: \n" +
//                "(A) THE COMPANY DISCLAIMS ALL LIABILITY WHATSOEVER, WHETHER ARISING IN CONTRACT, NEGLIGENCE OR OTHERWISE IN RELATION TO THE APP; AND \n" +
//                "(B) ALL IMPLIED WARRANTIES, TERMS AND CONDITIONS RELEVANT TO THE APP (WHETHER IMPLIED BY STATUE, COMMON LAW OR ANY OTHER), INCLUDING (WITHOUT LIMITATION) ANY WARRANTY, TERM OR CONDITION AS TO ACCURACY, PERFORMANCE, COMPLETENESS, SATISFACTORY QUALITY, FITNESS FOR ANY OR SPECIAL PURPOSE, AVAILABILITY, NON INFRINGEMENT, INFORMATION ACCURACY, INTEROPERABILITY, QUIET USAGE AND TITLE ARE, AS BETWEEN THE COMPANY AND YOU, HEREBY EXCLUDED. IN PARTICULAR, BUT WITHOUT PREJUDICE TO THE FOREGOING, THE COMPANY ACCEPT NO RESPONSIBILITY FOR ANY TECHNICAL FAILURE OF THE INTERNET AND/OR THE APP; OR ANY DAMAGE OR ECONOMIC LOSS (INCLUDING, WITHOUT LIMITATION, LOSS OF MONEY, PROFITS, OR INJURY TO USERS OR THEIR EQUIPMENT AS A RESULT OF OR RELATING TO THEIR USE OF THE APP. YOUR STATUTORY RIGHTS ARE NOT AFFECTED. \n" +
//                "(C) IF THE COMPANY IS LIABLE TO YOU DIRECTLY OR INDIRECTLY IN RELATION TO THE APP, THAT LIABILITY (HOWSOEVER ARISING) IS LIMITED TO THE SUM PAID BY YOU AT THE TIME OF REGISTRATION OF THE APP, OR ANY IN-APP PURCHASE, INCLUDING SUBSCRIPTIONS, WHICHEVER IS GREATER.\n" +
//                "(D) REFUND POLICY: NO REFUND WILL BE GIVEN UNDER ANY CIRCUMSTANCES TO ANY USER IF THE USER REGISTERS AND USES THE CONTENT OF THE APPLICATION AS THE APP CONTAINS DIGITAL INFORMATION. HOWEVER IF THE REFUND IS REQUESTED ON THE BASIS OF TRANSACTION PROBLEMS OR SIMILAR TYPES OF ISSUES, DEPENDING UPON THE SITUATION REFUND MAY BE APPROVED BY THE COMPANY AND WILL BE EFFECTED WITHIN 7 DAYS OF GETTING PROPER REQUEST THROUGH THE CONPANY’S E-MAIL ID FOR THE PURPOSE.\n" +
//                "(E) AS OF NOW, THE REGISTERED USERS WHO MADE THE REQUIRED PAYMENT CAN USE ALL THE MATERIALS AVAILABLE IN THE APP AND THE APP WILL BE AD-FREE. TO IDENTIFY AND PROMOTE UNIQUE USERS OF THE APP AS WELL AS UNAUTHORISED APP USAGE, REGISTRATION IS DONE BASED ON THE UNIQUE DEVICE ID-EMAIL ID COMBINATION. IF YOU CHANGE THE DEVICE ON WHICH THE APP IS INSTALLED YOU WILL HAVE TO REQUEST THE COMPANY TO GET IT ACTIVATED ON THE NEW DEVICE. ACTIVATION OF THE APP ON THE NEW DEVICE IS THE SOLE DISCRETION OF THE COMPANY AND ACTIVATION WILL BE DONE FOR THE SAME EMAIL ID-NEW UNIQUE DEVICE ID COMBINATION. YOU WILL NOT GET THE APP ACTIVATED FOR A NEW EMAIL ID IN ANY CIRCUMSTANCES. \n" +
//                "9.\tSUSPENSION OF SERVICE: The Company reserves the right to suspend or stop providing any services relating to the App, with or without notice, and shall have no liability or responsibility to you in any manner.\n" +
//                "10.\tIN-THE-APP ADVERTISEMENTS: We accept no responsibility for advertisements or the subsequent goods purchase or services in the advertisements contained within the App and the advertiser, not the Company, is responsible for any issues related to such goods and/or services.\n" +
//                "11.\t DEVICES AND CONNECTIVITY: The provision of the Mobile Application does not include the provision of a mobile or device or other necessary equipment to access the App or the Services. To use the App or Services, you will require internet connectivity and appropriate devices. You acknowledge that the terms of agreement with your respective internet provider will continue to apply when using the App. As a result, you may be charged by the internet service provider for access to network connection services for the duration of the connection while accessing the App or any such third party charges as may arise. You accept responsibility for any such charges that arise.\n" +
//                "\n" +
//                "The above Terms constitute the entire agreement between you and the Company concerning your use of the App.\n" +
//                "The Company reserves the right to append/update these Terms from time to time whenever necessary. In such cases, the updated Terms will be coming to effect immediately, and the current Terms will be available through the App. You are supposed to regularly review these Terms so that you will come to know changes to them and you will be bound by the new policy upon your continued use of the App. No other variation to these Terms shall be effective unless in writing and signed by an authorised representative on behalf of the Company.\n" +
//                "These Terms shall be governed by and construed in accordance with Indian law and you agree to submit to the exclusive jurisdiction of the Indian Courts.\n" +
//                "If any provision(s) of these Terms is held by a court of competent jurisdiction to be invalid or unenforceable, then such provision(s) shall be construed, as nearly as possible, to reflect the intentions of the parties (as reflected in the provision(s)) and all other provisions shall remain in full force and effect.\n" +
//                "The Company’s failure to exercise or enforce any right or provision of these Terms shall not constitute a waiver of such right or provision unless acknowledged and agreed to by the Company in writing.\n");
    }

}
