package com.splogics.enggmechanicslite.Fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.splogics.enggmechanicslite.Adapter.CategoryAdapter;
import com.splogics.enggmechanicslite.MyActivity;
import com.splogics.enggmechanicslite.R;
import com.splogics.enggmechanicslite.SignUpActivity;
import com.splogics.enggmechanicslite.WebService.GetAllCategories;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FirstPageFragment extends Fragment {

    public static FirstPageFragment object;

    public static FirstPageFragment getObject() {
        return object;
    }

    private Button btnReadnow;
    private GridView gridView;
    private ProgressBar progressBar;

    CategoryAdapter categoryAdapter;
    ArrayList<String> mcategoryNames;
    ArrayList<String> mcategoryImages;
    ArrayList<Integer> mcatergoryID;
    ArrayList<Integer> paymentPosition;
    Button clear;
    TextView firstpageTitle;
    AlertDialog ad;

    InterstitialAd mInterstitialAd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.activity_first_page_fragment, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState)
    {

        // getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        object = this;
        mcategoryImages = new ArrayList<>();
        mcategoryNames = new ArrayList<>();
        mcatergoryID = new ArrayList<>();
        paymentPosition = new ArrayList<>();

        //calling ads to load
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-8638167269558284/4458202456");

        mInterstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        requestNewInterstitial();

        // calling webservice to load datas
        GetAllCategories loadingCategories = new GetAllCategories();
        loadingCategories.execute();

        progressBar = (ProgressBar) view.findViewById(R.id.first_loader);

       /* firstpageTitle = (TextView) view.findViewById(R.id.firstpage_title);
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Chantelli_Antiqua.ttf");
        firstpageTitle.setTypeface(typeface);*/

        gridView = (GridView) view.findViewById(R.id.firstpage_gridview);
        categoryAdapter = new CategoryAdapter(getContext(), mcategoryNames, mcategoryImages, paymentPosition);
        gridView.setAdapter(categoryAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showAdvs();
                movetonext_fragment(position);
            }
        });
    }


    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void showAdvs()
    {
        // this is here were paid users are identified
        SharedPreferences sharedPreferences_loggedUser = getContext().getSharedPreferences("MyLogDetails", MODE_PRIVATE);
        Boolean paid_OR_not = sharedPreferences_loggedUser.getBoolean("paidStatus",false);
        if(!paid_OR_not) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        }
    }

    public void pop_up(final int posi) {

        try {
            //calling advertisment
                 showAdvs();

                AlertDialog.Builder alertadd = new AlertDialog.Builder(getContext());
                LayoutInflater factory = LayoutInflater.from(getContext());
                final View view = factory.inflate(R.layout.confirmlayout, null);
                TextView textViewshopname = (TextView) view.findViewById(R.id.confirm_textview);

                textViewshopname.setText("Premium content,only premium users can view.Upgrade to pro ?");
                Button btnYes = (Button) view.findViewById(R.id.confirm_btnYes);
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ad.dismiss();

                        SharedPreferences sharedPreferences_loggedUser = getContext().getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                        int guestStatus = sharedPreferences_loggedUser.getInt("guestStatus", 0);
                        if (guestStatus == 1)
                        {
                            Bundle b = new Bundle();
                            b.putInt("temp",1);
                            Intent intent = new Intent(getActivity(), SignUpActivity.class);
                            intent.putExtras(b);
                            getActivity().startActivity(intent);

                        }
                        else
                        {
                            Intent intent = new Intent(getActivity(), MyActivity.class);
                            getActivity().startActivity(intent);
                        }
                    }
                });
                Button btnNo = (Button) view.findViewById(R.id.confirm_btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ad.dismiss();
                    }
                });

                alertadd.setView(view);
                ad = alertadd.show();

        } catch (Exception exeArray) {
            Log.d("First@exe", "" + exeArray);
        }
    }


    public void movetonext_fragment(int posi) {

//        calling advertisment
          showAdvs();

        if (paymentPosition.get(posi) != 1)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("book_categoryID", mcatergoryID.get(posi));
            Fragment myFragment = new FragmentMain();
            myFragment.setArguments(bundle);
            FragmentManager manager = ((AppCompatActivity) getContext()).getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_frame, myFragment);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            String backStateName = FirstPageFragment.class.getName();
            transaction.addToBackStack(backStateName);
            transaction.commit();
        }
        else
        {
//            this is here were paid users are identified
            SharedPreferences sharedPreferences_loggedUser = getContext().getSharedPreferences("MyLogDetails", MODE_PRIVATE);
            Boolean paid_OR_not = sharedPreferences_loggedUser.getBoolean("paidStatus",false);
            if (paid_OR_not)
            {
                Bundle bundle = new Bundle();
                bundle.putInt("book_categoryID", mcatergoryID.get(posi));
                Fragment myFragment = new FragmentMain();
                myFragment.setArguments(bundle);
                FragmentManager manager = ((AppCompatActivity) getContext()).getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.content_frame, myFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                String backStateName = FirstPageFragment.class.getName();
                transaction.addToBackStack(backStateName);
                transaction.commit();

            }
            else
            {
                pop_up(posi);
            }
//            Toast.makeText(getContext(), "Upgrade", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(getActivity(), MyActivity.class);
//            getActivity().startActivity(intent);
        }
    }

    public void setCategory_fromweb(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            int size = jsonArray.length();
            if (size > 0)
            {
                //clearing intail datas to avoid repeating data
                mcategoryImages.clear();
                mcategoryNames.clear();
                mcatergoryID.clear();
                paymentPosition.clear();

                for (int i = size-1; i > -1; i--)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    mcategoryNames.add(jsonObject.getString("Category"));
                    mcatergoryID.add(jsonObject.getInt("PK_CategoryID"));
                    mcategoryImages.add(jsonObject.getString("ImageFile"));
                    paymentPosition.add(jsonObject.getInt("PaymentStatus"));
                }
            }
            progressBar.setVisibility(View.GONE);
            categoryAdapter.notifyDataSetChanged();
        }
        catch (Exception exp) {
            Log.d("Exe@firstpagefragment", "" + exp);
        }
    }
}
