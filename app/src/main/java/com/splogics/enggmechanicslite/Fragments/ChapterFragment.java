package com.splogics.enggmechanicslite.Fragments;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.splogics.enggmechanicslite.Adapter.ChapterNameAdapter;
import com.splogics.enggmechanicslite.R;
import com.splogics.enggmechanicslite.WebService.GetAllChapters;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class ChapterFragment extends Fragment {

    private RecyclerView mRecyclerViewChapter;
    private ArrayList<String> moduleNames;
    private ArrayList<Integer> moduleID;
    private RecyclerView.LayoutManager mLayoutManager;
    private int selectedBookID;
    private String selectedBookTitle="Book Name";
    private ChapterNameAdapter madapter;
    public static ChapterFragment chapterObj;

    InterstitialAd mInterstitialAd;

    public static ChapterFragment getObject() {
        return chapterObj;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_chapter_fragment, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        Bundle b = getArguments();
        if (b != null) {
            selectedBookID = b.getInt("BookID_toRead");
            selectedBookTitle = b.getString("BookTitle_toRead");
        }

        //initializing the object

        chapterObj = this;
        moduleNames = new ArrayList<>();
        moduleID = new ArrayList<>();

        //calling ads to load
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-8638167269558284/4458202456");

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        requestNewInterstitial();

        //calling webservice to get the chapter based on the selected book from previous fragments :)
        GetAllChapters chapters = new GetAllChapters();
        chapters.addBookID("" + selectedBookID);
        chapters.execute();

        showAdvs();

        TextView mbooknamesSelected = (TextView)view.findViewById(R.id.chapter_bookName) ;
        mbooknamesSelected.setText(selectedBookTitle);
        mRecyclerViewChapter = (RecyclerView) view.findViewById(R.id.chapter_recycler);
        mRecyclerViewChapter.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerViewChapter.setLayoutManager(mLayoutManager);
        madapter = new ChapterNameAdapter(moduleNames, moduleID, getContext(),selectedBookID);
        mRecyclerViewChapter.setAdapter(madapter);

    }


    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }


    private void showAdvs()
    {
        // this is here were paid users are identified
        SharedPreferences sharedPreferences_loggedUser = getContext().getSharedPreferences("MyLogDetails", MODE_PRIVATE);
        Boolean paid_OR_not = sharedPreferences_loggedUser.getBoolean("paidStatus",false);
        if(!paid_OR_not) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        }
    }


    public void setChapterBasedOnBookID(String response) {

        try {
            moduleID.clear();
            moduleNames.clear();
            JSONArray jsonArray = new JSONArray(response);
            int size = jsonArray.length();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    moduleID.add(jsonObject.getInt("ChapterID"));
                    moduleNames.add(jsonObject.getString("Chapter"));
                }
                madapter.notifyDataSetChanged();
            }
        } catch (Exception eg) {
            Log.d("ChapterFrag", "Exception" + eg);
        }

    }


}
