package com.splogics.enggmechanicslite.Fragments;

import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.splogics.enggmechanicslite.R;
import com.splogics.enggmechanicslite.WebService.GetBookContent;

import org.json.JSONArray;
import org.json.JSONObject;

public class DataViewPage extends Fragment {

    private WebView mWebView;
    private ProgressBar mProgressBar;
    private FloatingActionButton btnNext;
    private ImageView image_flip;
    private int bookID,chapterID;
    private static DataViewPage obj;
    public static DataViewPage getObject()
    {
        return obj;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.activity_data_view_page, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        //intailizing object

        obj = this;


        image_flip = (ImageView) view.findViewById(R.id.flip);
        btnNext = (FloatingActionButton) view.findViewById(R.id.data_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_flip.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        image_flip.setVisibility(View.GONE);
                    }
                }, 500);
            }
        });


        //getting bundle with chapter and book

        Bundle bundle = getArguments();
        if (bundle != null) {
            bookID =  bundle.getInt("bookID");
            chapterID = bundle.getInt("chapterID");

       //calling webservice to get data based in book and chapter Id;
            GetBookContent getBookContent=new GetBookContent();
            getBookContent.addBookChapterID(bookID,chapterID);
            getBookContent.execute();


        }

       // final String url_main = "http://192.168.1.202:8099/index.htm";
        mProgressBar = (ProgressBar) view.findViewById(R.id.webview_progress);
        mWebView = (WebView) view.findViewById(R.id.webview_container);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDefaultTextEncodingName("utf-8");

//        mWebView.setOnTouchListener(new View.OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return (event.getAction() == MotionEvent.ACTION_MOVE);
//            }
//        });
//        mWebView.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return false;
//            }
//        });
//        mWebView.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageFinished(WebView view, String url) {
//
//                if (url.contains(url_main)) {
//                    mWebView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
//                }
//            }
//        });

        //mWebView.loadUrl(url_main);

        mWebView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getContext(),"hii",Toast.LENGTH_SHORT).show();
            }
        });
        mWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        mWebView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                    return true;
            }
        });
        mWebView.setLongClickable(false);

    }

    class MyJavaScriptInterface {
        @JavascriptInterface
        public void processHTML(final String html) {
            Log.i("processed html", html);

            Thread OauthFetcher = new Thread(new Runnable() {

                @Override
                public void run() {

                    String oAuthDetails = null;
                    oAuthDetails = Html.fromHtml(html).toString();
                    Log.i("oAuthDetails", oAuthDetails);
                }
            });
            OauthFetcher.start();
        }
    }



    //webservice respose function starts here :

    public void setBookContent(String respose)
    {
        Log.d("DataRes",""+respose);
        String summary = "<html><body><b> Data connection lost, please try again </b></body></html>";
        try {
            JSONArray jsonArray = new JSONArray(respose);
            int size = jsonArray.length();
            if (size > 0)
            {
                for (int i = 0; i < size; i++)
                {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                summary = jsonObject.getString("PageContent");
                }
            }
            mProgressBar.setVisibility(View.GONE);
        }
        catch (Exception exception)
        {

        }
        mWebView.loadData(summary, "text/html; charset=utf-8", null);

    }


}
