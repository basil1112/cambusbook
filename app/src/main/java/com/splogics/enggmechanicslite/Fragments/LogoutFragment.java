package com.splogics.enggmechanicslite.Fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splogics.enggmechanicslite.LoginActivity;
import com.splogics.enggmechanicslite.R;

import static android.content.Context.MODE_PRIVATE;


public class LogoutFragment extends Fragment {

    SharedPreferences sh;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_logout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        sh = getActivity().getSharedPreferences("MyLogDetails", MODE_PRIVATE);
        SharedPreferences.Editor ed = sh.edit();
        ed.putBoolean("Logged", true);
        ed.putString("LoggedUser_userID","");
        ed.putString("LoggedUser_Email","");
        ed.putString("LoggedUser_Name","");
        ed.putString("LoggedUser_Phone","");
        ed.putString("LoggedUser_AndroidID","");
        ed.putBoolean("paidStatus",false);
        ed.commit();


        getOut();

    }
    public void getOut()
    {
      Intent intent = new Intent(getContext(),LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
