package com.splogics.enggmechanicslite;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.splogics.enggmechanicslite.WebService.CheckUserExistance;
import com.splogics.enggmechanicslite.WebService.InsertUserDetails;

import org.json.JSONObject;

public class SignUpActivity extends AppCompatActivity {
    private EditText txtName, txtEmail, txtMobile;
    private Button btnSignUp;
    private RelativeLayout relativeLayoutMsg;
    SharedPreferences sh;
    public Bundle getBundle = null;
    int tempvalue = 0;
    AlertDialog ad;
    private ProgressBar progressBar;
    public static SignUpActivity object;
    private TextView textView, txtviewpop;
    private RelativeLayout txtloginlayout;

    public static SignUpActivity getObject() {
        return object;
    }

    String _name, _email, _mobilenumber, _myAndroidDeviceId;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        object = this;
        relativeLayoutMsg = (RelativeLayout) findViewById(R.id.signup_msgbox);
        txtName = (EditText) findViewById(R.id.input_name);
        txtEmail = (EditText) findViewById(R.id.input_email);
        txtMobile = (EditText) findViewById(R.id.input_mob);
        progressBar = (ProgressBar) findViewById(R.id.signup_progress);
        txtviewpop = (TextView) findViewById(R.id.textViewpop);
        txtviewpop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pop_up();
            }
        });
        textView = (TextView) findViewById(R.id.already_btn);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });
        txtloginlayout = (RelativeLayout) findViewById(R.id.txtloginlayout);
        txtloginlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });

        getBundle = this.getIntent().getExtras();
        if (getBundle != null)
        {
            tempvalue = getBundle.getInt("temp");
        }


        btnSignUp = (Button) findViewById(R.id.btnSignup);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                //getting user input datas on button click
                _myAndroidDeviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                _name = txtName.getText().toString();
                _email = txtEmail.getText().toString();
                _mobilenumber = txtMobile.getText().toString();

                if (_name.equals(""))
                {
                    txtName.setError("Name cannot be Blank");
                } else if (_email.equals(""))
                {
                    txtEmail.setError("Email cannot be Blank");
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(_email).matches())
                {
                    txtEmail.setError("Enter valid email format");
                } else if (_mobilenumber.equals(""))
                {
                    txtMobile.setError("Mobilenumber cannot be Blank");
                } else if (_mobilenumber.length() < 10)
                {
                    txtMobile.setError("Enter valid mobile number");
                } else {
                    //calling already exists webservice

                    ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                    if(isConnected)
                    {

                        //sign up button is disabled
                        btnSignUp.setEnabled(false);


                        CheckUserExistance checkUserExistance = new CheckUserExistance();
                        checkUserExistance.checkingUsername(_email);
                        checkUserExistance.execute();
                        progressBar.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"You are not connected to internet, please enable connectivity and try again",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public void pop_up() {

        AlertDialog.Builder alertadd = new AlertDialog.Builder(SignUpActivity.this);
        LayoutInflater factory = LayoutInflater.from(SignUpActivity.this);
        final View view = factory.inflate(R.layout.terms_conditions, null);
        TextView textViewshopname = (TextView) view.findViewById(R.id.agreement_textview);
        textViewshopname.setText(Html.fromHtml(
                "<h2><strong>Engineering Mechanics App </strong></h2>\n" +
                        "\n" +
                        "<p>Terms and Conditions<br />\n" +
                        "ABOUT THE APPLICATION:</p>\n" +
                        "\n" +
                        "<p>A warm welcome to our mobile application - Engineering Mechanics (the &ldquo;App&rdquo;). This App is developed and maintained by SP Logic Technologies (P) Ltd. Kochi, Kerala, India (the &ldquo;Company&rdquo;).&nbsp;</p>\n" +
                        "\n" +
                        "<p>THE APP PRIVACY POLICY:</p>\n" +
                        "\n" +
                        "<p>When you register with the Company and use the App, you will provide your name, email address, mobile number, transaction-related information, information you provide the Company when you contact the Company for help and credit card information for purchase. We collect device IDs for registration as well as for updates for better experience of the App in future. However, the Company assures the users that the data you provided as well as the data the App collected will not be used for any other purpose other than specified in the privacy policy of the App. We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorised access, disclosure, copying, use or modification.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "AGREEMENT:</p>\n" +
                        "\n" +
                        "<p>If you download or install the App on your device(s), you agree to the terms and conditions (the &ldquo;Terms&rdquo;) of the App and the privacy policy of the App. If you are not agreeing to the Terms or not satisfied with the privacy policy, you should immediately stop using it and should uninstall the App from your device(s). To help you with doubts and confusions, you can contact through our e-mail: logixsp@gmail.com. Any continued use of the App or services of the App means you agree to the Terms and satisfied by the privacy policy of the App.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "GENERAL CONDITIONS:</p>\n" +
                        "\n" +
                        "<p>The App is published and is available to you for your own personal use only. It should not be used for public or commercial purpose. It is strictly restricted to use the App for any illegal, fraudulent, unlawful or harmful purposes including (without limitation) copyright infringement. You are not supposed to use this app for any marketing purposes without the permission from the company. Also the user must comply with any applicable international laws, including the local laws in the user&rsquo;s country.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "CONTENT:</p>\n" +
                        "\n" +
                        "<p>The copyright in all content materials in the App including all information, data, text, audio, photographs, graphics and videos and other material (the &ldquo;Material&ldquo;) is owned by or licensed to the Company and all rights of the mentioned are reserved. You can view the Material for your own personal use but you cannot otherwise copy, modify, change, re-produce, re-publish, display publicly, re-distribute, save, exchange or commercially exploit in any form whatsoever or use the Material without the Company&rsquo;s explicit permission.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "THE DOs and DONTs:</p>\n" +
                        "\n" +
                        "<p>You agree that when using the App you will strictly adhere to all the applicable laws and these Terms. In particular, but without limitation, you agree not to:<br />\n" +
                        "Intentional attempt to get unauthorised access to the App or any networks, servers or computer systems directly or indirectly connected to the App.<br />\n" +
                        "Modify, adapt, translate or reverse engineer any part of the App or re-format or frame any portion of the pages displayed in the App, save to the extent expressly permitted by these Terms or by law.<br />\n" +
                        "You agree to indemnify the Company in full and on demand from and against any loss, damage, costs or expenses which they suffer or incur directly or indirectly as a result of your use of the App otherwise than in accordance with these Terms or applicable laws.<br />\n" +
                        "THE LOGOS: The logos (the &ldquo;Logos&ldquo;) used in the App are owned by the Company. You cannot use, copy, edit, vary, reproduce, publish, display, distribute, store, transmit, commercially exploit or disseminate the Logos without the prior written consent of the Company.</p>\n" +
                        "\n" +
                        "<p><br />\n" +
                        "LIMITATIONS OF LIABILITY:</p>\n" +
                        "\n" +
                        "<p>THE APP IS PROVIDED ON AN &ldquo;AS IS&rdquo; BASIS. USE OF THE APP IS AT YOUR OWN RISK. TO THE MAXIMUM EXTENT PERMITTED BY LAW:&nbsp;<br />\n" +
                        "(A) THE COMPANY DISCLAIMS ALL LIABILITY WHATSOEVER, WHETHER ARISING IN CONTRACT, NEGLIGENCE OR OTHERWISE IN RELATION TO THE APP; AND&nbsp;<br />\n" +
                        "(B) ALL IMPLIED WARRANTIES, TERMS AND CONDITIONS RELEVANT TO THE APP (WHETHER IMPLIED BY STATUE, COMMON LAW OR ANY OTHER), INCLUDING (WITHOUT LIMITATION) ANY WARRANTY, TERM OR CONDITION AS TO ACCURACY, PERFORMANCE, COMPLETENESS, SATISFACTORY QUALITY, FITNESS FOR ANY OR SPECIAL PURPOSE, AVAILABILITY, NON INFRINGEMENT, INFORMATION ACCURACY, INTEROPERABILITY, QUIET USAGE AND TITLE ARE, AS BETWEEN THE COMPANY AND YOU, HEREBY EXCLUDED. IN PARTICULAR, BUT WITHOUT PREJUDICE TO THE FOREGOING, THE COMPANY ACCEPT NO RESPONSIBILITY FOR ANY TECHNICAL FAILURE OF THE INTERNET AND/OR THE APP; OR ANY DAMAGE OR ECONOMIC LOSS (INCLUDING, WITHOUT LIMITATION, LOSS OF MONEY, PROFITS, OR INJURY TO USERS OR THEIR EQUIPMENT AS A RESULT OF OR RELATING TO THEIR USE OF THE APP. YOUR STATUTORY RIGHTS ARE NOT AFFECTED.&nbsp;<br />\n" +
                        "(C) IF THE COMPANY IS LIABLE TO YOU DIRECTLY OR INDIRECTLY IN RELATION TO THE APP, THAT LIABILITY (HOWSOEVER ARISING) IS LIMITED TO THE SUM PAID BY YOU AT THE TIME OF REGISTRATION OF THE APP, OR ANY IN-APP PURCHASE, INCLUDING SUBSCRIPTIONS, WHICHEVER IS GREATER.<br />\n" +
                        "(D) REFUND POLICY: NO REFUND WILL BE GIVEN UNDER ANY CIRCUMSTANCES TO ANY USER IF THE USER REGISTERS AND USES THE CONTENT OF THE APPLICATION AS THE APP CONTAINS DIGITAL INFORMATION. HOWEVER IF THE REFUND IS REQUESTED ON THE BASIS OF TRANSACTION PROBLEMS OR SIMILAR TYPES OF ISSUES, DEPENDING UPON THE SITUATION REFUND MAY BE APPROVED BY THE COMPANY AND WILL BE EFFECTED WITHIN 7 DAYS OF GETTING PROPER REQUEST THROUGH THE CONPANY&rsquo;S E-MAIL ID FOR THE PURPOSE.<br />\n" +
                        "(E) AS OF NOW, THE REGISTERED USERS WHO MADE THE REQUIRED PAYMENT CAN USE ALL THE MATERIALS AVAILABLE IN THE APP AND THE APP WILL BE AD-FREE. TO IDENTIFY AND PROMOTE UNIQUE USERS OF THE APP AS WELL AS UNAUTHORISED APP USAGE, REGISTRATION IS DONE BASED ON THE UNIQUE DEVICE ID-EMAIL ID COMBINATION. IF YOU CHANGE THE DEVICE ON WHICH THE APP IS INSTALLED YOU WILL HAVE TO REQUEST THE COMPANY TO GET IT ACTIVATED ON THE NEW DEVICE. ACTIVATION OF THE APP ON THE NEW DEVICE IS THE SOLE DISCRETION OF THE COMPANY AND ACTIVATION WILL BE DONE FOR THE SAME EMAIL ID-NEW UNIQUE DEVICE ID COMBINATION. YOU WILL NOT GET THE APP ACTIVATED FOR A NEW EMAIL ID IN ANY CIRCUMSTANCES.&nbsp;<br />\n" +
                        "SUSPENSION OF SERVICE: The Company reserves the right to suspend or stop providing any services relating to the App, with or without notice, and shall have no liability or responsibility to you in any manner.</p>\n" +
                        "\n" +
                        "<p>IN-THE-APP ADVERTISEMENTS:</p>\n" +
                        "\n" +
                        "<p>We accept no responsibility for advertisements or the subsequent goods purchase or services in the advertisements contained within the App and the advertiser, not the Company, is responsible for any issues related to such goods and/or services.<br />\n" +
                        "&nbsp;DEVICES AND CONNECTIVITY: The provision of the Mobile Application does not include the provision of a mobile or device or other necessary equipment to access the App or the Services. To use the App or Services, you will require internet connectivity and appropriate devices. You acknowledge that the terms of agreement with your respective internet provider will continue to apply when using the App. As a result, you may be charged by the internet service provider for access to network connection services for the duration of the connection while accessing the App or any such third party charges as may arise. You accept responsibility for any such charges that arise.</p>\n" +
                        "\n" +
                        "<p>The above Terms constitute the entire agreement between you and the Company concerning your use of the App.<br />\n" +
                        "The Company reserves the right to append/update these Terms from time to time whenever necessary. In such cases, the updated Terms will be coming to effect immediately, and the current Terms will be available through the App. You are supposed to regularly review these Terms so that you will come to know changes to them and you will be bound by the new policy upon your continued use of the App. No other variation to these Terms shall be effective unless in writing and signed by an authorised representative on behalf of the Company.<br />\n" +
                        "These Terms shall be governed by and construed in accordance with Indian law and you agree to submit to the exclusive jurisdiction of the Indian Courts.<br />\n" +
                        "If any provision(s) of these Terms is held by a court of competent jurisdiction to be invalid or unenforceable, then such provision(s) shall be construed, as nearly as possible, to reflect the intentions of the parties (as reflected in the provision(s)) and all other provisions shall remain in full force and effect.<br />\n" +
                        "The Company&rsquo;s failure to exercise or enforce any right or provision of these Terms shall not constitute a waiver of such right or provision unless acknowledged and agreed to by the Company in writing.</p>\n "

        ));
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.advclose);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });

        alertadd.setView(view);
        ad = alertadd.show();

    }

    public void register_success(String respose) {
        try {
            int resposevalue = Integer.parseInt(respose);
            if (resposevalue > 0) {
                Toast.makeText(SignUpActivity.this, "Sign Up Success", Toast.LENGTH_SHORT).show();
                sh = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                SharedPreferences.Editor ed = sh.edit();
                //this true makes the app one time registeration succesfull status
                ed.putBoolean("Logged", true);
                ed.putString("LoggedUser_userID", "" + resposevalue);
                ed.putString("LoggedUser_Email", _email);
                ed.putString("LoggedUser_Name", _name);
                ed.putString("LoggedUser_Phone", _mobilenumber);
                ed.putString("LoggedUser_AndroidID", _myAndroidDeviceId);

                if (tempvalue != 0) {
                    ed.putInt("guestStatus", 2);
                }

                ed.commit();
                sh = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                int gueststatus = sh.getInt("guestStatus", -1);

                if (gueststatus == 2) {
                    startActivity(new Intent(SignUpActivity.this, MyActivity.class));
                    finish();

                } else if (gueststatus >= 0) {
                    sh = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                    SharedPreferences.Editor ed1 = sh.edit();
                    ed1.putInt("guestStatus", 0);

                    startActivity(new Intent(SignUpActivity.this, HomeActivity.class));
                    finish();
                } else {
                    sh = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                    SharedPreferences.Editor ed1 = sh.edit();
                    ed1.putInt("guestStatus", 0);
                    startActivity(new Intent(SignUpActivity.this, HomeActivity.class));
                    finish();
                }
            } else {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(SignUpActivity.this, "Sign Up Failed ! Try After Some Minutes", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception exp) {
            Toast.makeText(SignUpActivity.this, "Sign Up Failed ! Try After Some Minutes", Toast.LENGTH_SHORT).show();
            Log.d("Signupservice@error", "" + exp);
        }
    }


    public void user_exists(String respose) {
        String alreadyExists = "false";
        try {

            JSONObject jsonObject = new JSONObject(respose);
            alreadyExists = jsonObject.getString("UserExistance");

            if (alreadyExists.equals("false"))
            {
                //webservice caller for register and sharedprefrence editor (one time sign up )
                InsertUserDetails insertUserDetails = new InsertUserDetails();
                insertUserDetails.addUserDetails(_name, _email, _mobilenumber, _myAndroidDeviceId);
                insertUserDetails.execute();
            }
            else
            {
                btnSignUp.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                relativeLayoutMsg.setVisibility(View.VISIBLE);
                blink();
            }

        }
        catch (Exception eg)
        {
            Log.d("Signup", "Exception" + eg);
        }
    }

    public void blink() {

        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinkthree);
        relativeLayoutMsg.startAnimation(animation1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                relativeLayoutMsg.setVisibility(View.INVISIBLE);
            }
        }, 2000);
    }

}
