package com.splogics.enggmechanicslite.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.splogics.enggmechanicslite.Fragments.DataViewPage;
import com.splogics.enggmechanicslite.R;

import java.util.ArrayList;


public class ChapterNameAdapter extends RecyclerView.Adapter<ChapterNameAdapter.viewhold> {

    private int selectedBookID;
    private ArrayList<String> mModuleNames;
    private ArrayList<Integer> mModuleID;
    private Context mContext;
    SharedPreferences msharedPreferences;


    public ChapterNameAdapter(ArrayList<String> data, ArrayList<Integer> modID_data, Context context, int bookID) {

        mModuleNames = data;
        mModuleID = modID_data;
        mContext = context;
        selectedBookID = bookID;
    }

    @Override
    public int getItemCount() {

        return mModuleNames.size();
    }

    @Override
    public viewhold onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_singlerow, parent, false);
        viewhold vh = new viewhold(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(viewhold holder, int position) {

        holder.mtextview.setText(mModuleNames.get(position));
//        if((position % 3) == 0) {
//            holder.mimageview.setImageResource(R.mipmap.boyprofilenew);
//        }

    }


    public class viewhold extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mtextview;
        ImageView mimageview;
        LinearLayout mCardview;

        // alertbox for conformation yes or no in shop selection;
        AlertDialog ad;

        public viewhold(View itemView) {
            super(itemView);

            itemView.setClickable(true);
            itemView.setOnClickListener(this);
            TextView textView = (TextView) itemView.findViewById(R.id.txtChapterName);
            mtextview = textView;
            mCardview = (LinearLayout) itemView.findViewById(R.id.chapter_module_cardview);
            mCardview.setOnClickListener(this);
//            ImageView imageView = (ImageView) itemView.findViewById(R.id.shop_imageview);
//            mimageview = imageView;

        }

        @Override
        public void onClick(View v) {
            if (v == mCardview) {
                int clikedPosition = getAdapterPosition();
               // pop_up(clikedPosition);
                movetonext_fragment(clikedPosition);
            }

        }

        public void pop_up(final int posi) {
            String str = mModuleNames.get(posi);

            AlertDialog.Builder alertadd = new AlertDialog.Builder(itemView.getContext());
            LayoutInflater factory = LayoutInflater.from(itemView.getContext());
            final View view = factory.inflate(R.layout.confirmlayout, null);
            TextView textViewshopname = (TextView) view.findViewById(R.id.confirm_textview);

            textViewshopname.setText("Do You want to open " + str);
            Button btnYes = (Button) view.findViewById(R.id.confirm_btnYes);
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ad.dismiss();
                    //this is method which moves the fragment to order fragment carying the shop names/id
                    movetonext_fragment(posi);

                }
            });
            Button btnNo = (Button) view.findViewById(R.id.confirm_btnNo);
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ad.dismiss();
                }
            });

            alertadd.setView(view);
            ad = alertadd.show();

        }

        public void movetonext_fragment(int position) {

            Fragment myFragment = new DataViewPage();
            FragmentManager manager = ((AppCompatActivity) mContext).getSupportFragmentManager();

            Bundle getData_fromchapter_book = new Bundle();
            getData_fromchapter_book.putInt("bookID", selectedBookID);
            getData_fromchapter_book.putInt("chapterID", mModuleID.get(position));
            myFragment.setArguments(getData_fromchapter_book);

//            msharedPreferences = ((AppCompatActivity) mContext).getSharedPreferences("myShared_shopname", MODE_PRIVATE);
//            SharedPreferences.Editor editor = msharedPreferences.edit();
//            editor.putInt("module_id", mModuleID.get(position));
//            editor.apply();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_frame, myFragment);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
