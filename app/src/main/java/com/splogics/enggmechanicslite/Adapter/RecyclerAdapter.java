package com.splogics.enggmechanicslite.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.splogics.enggmechanicslite.R;

import java.util.List;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RowViewHolder>{
    private List<String> names;

    public RecyclerAdapter(List<String> names)
    {
        this.names = names;
        notifyDataSetChanged();
    }

    @Override
    public RowViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.single_recycler_row, viewGroup, false);
        return new RowViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RowViewHolder rowViewHolder, int i) {
        rowViewHolder.textView.setText(names.get(i));
    }

    @Override
    public int getItemCount() {
        
        return names.size();
    }

    class RowViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        public RowViewHolder(View itemView) {
            super(itemView);
          //  textView = (TextView) itemView.findViewById(R.id.contacts_text);
        }
    }
}
