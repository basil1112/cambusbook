package com.splogics.enggmechanicslite.Adapter;

import android.content.Context;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.splogics.enggmechanicslite.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class BooksAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Integer> mbookImages;
    private ArrayList<String> mbookTitle;
    private LayoutInflater mInflater;
    private ArrayList<String> mbookImagePath;
    private ArrayList<Integer> mbookPaymentStatus;
    int size = 0, extendedSize = 0;

    public BooksAdapter(Context context, ArrayList images, ArrayList names, ArrayList imagepath, ArrayList paymentStatus)
    {

        mContext = context;
        mbookImages = images;
        mbookTitle = names;
        mbookImagePath = imagepath;
        mbookPaymentStatus = paymentStatus;
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount()
    {
        size = mbookTitle.size();
//        if (size % 3 == 1) {
//            //first position so we need to add two blank books so as to complete the row of alamra :)
//            extendedSize = size + 2;
//        } else if (size % 3 == 2) {
//            //seconds position so we need to add two blank books so as to complete the row of alamra :)
//            extendedSize = size + 1;
//        } else {
//            extendedSize = size;
//        }

        return size;
    }

    @Override
    public Object getItem(int position) {
        return mbookTitle.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.single_recycler_row, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.recycle_book_title);
            viewHolder.ivImage = (ImageView) convertView.findViewById(R.id.recycle_book_img);
            viewHolder.ivBackdiv = (ImageView) convertView.findViewById(R.id.dividerback);
            viewHolder.payImg = (ImageView) convertView.findViewById(R.id.book_premium_img);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (position < size) {

            viewHolder.tvName.setText(mbookTitle.get(position));
//            Picasso.with(convertView.getContext())
//                    .load("http://192.168.1.202:8012/Uploads/CoverImages/" + mbookImagePath.get(position))
//                    .into(viewHolder.ivImage);
            Picasso.with(convertView.getContext())
                    .load("http://book.splogics.com/Uploads/CoverImages/" + mbookImagePath.get(position))
                    .into(viewHolder.ivImage);

            if (mbookPaymentStatus.get(position) != 1) {

                viewHolder.payImg.setVisibility(View.GONE);
            }
            else
            {

                //   this is here were paid users are identified
                SharedPreferences sharedPreferences_loggedUser = mContext.getSharedPreferences("MyLogDetails", MODE_PRIVATE);
                Boolean paid_OR_not = sharedPreferences_loggedUser.getBoolean("paidStatus",false);
                if (paid_OR_not)
                {
                    viewHolder.payImg.setVisibility(View.GONE);
                }

            }

//            if (position % 3 == 0) {
//                viewHolder.ivBackdiv.setImageResource(R.mipmap.sh1);
//            } else if (position % 3 == 1) {
//                viewHolder.ivBackdiv.setImageResource(R.mipmap.sh2);
//            } else {
//                viewHolder.ivBackdiv.setImageResource(R.mipmap.sh3);
//            }


        } else {
            viewHolder.tvName.setText("");
            viewHolder.payImg.setVisibility(View.GONE);
//            viewHolder.ivImage.setImageResource(R.mipmap.space);
//            if (position % 3 == 0) {
//
//                viewHolder.ivBackdiv.setImageResource(R.mipmap.sh1);
//            } else if (position % 3 == 1) {
//                viewHolder.ivBackdiv.setImageResource(R.mipmap.sh2);
//            } else {
//                viewHolder.ivBackdiv.setImageResource(R.mipmap.sh3);
//            }

        }
        return convertView;
    }

    static class ViewHolder {
        TextView tvName;
        ImageView ivImage;
        ImageView ivBackdiv;
        ImageView payImg;
    }
}
