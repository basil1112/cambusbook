package com.splogics.enggmechanicslite.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.splogics.enggmechanicslite.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sanoop on 23-12-16.
 */

public class CategoryAdapter extends BaseAdapter {

    private ArrayList<String> catNames;
    private ArrayList<String> catImages;
    private ArrayList<Integer> catPaymentStatus;
    private LayoutInflater mInflater;
    private Context mContext;

    public CategoryAdapter(Context context, ArrayList catNames, ArrayList catImages, ArrayList paymentStatus) {

        mContext=context;
        this.catNames = catNames;
        this.catImages = catImages;
        catPaymentStatus = paymentStatus;
        mInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return catNames.size();
    }

    @Override
    public Object getItem(int position) {
        return catNames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.single_category_row, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.cat_name_single);
            viewHolder.ivImage = (ImageView) convertView.findViewById(R.id.cat_image_single);
            viewHolder.payLayout = (RelativeLayout) convertView.findViewById(R.id.cat_lock);
            viewHolder.cardView = (CardView) convertView.findViewById(R.id.cat_cardview);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (catPaymentStatus.get(position) == 0) {
            //this product need to be paid  (in future check for user level payment status also) now simple
            viewHolder.payLayout.setVisibility(View.GONE);

        } else {

//            this is here were paid users are identified
             SharedPreferences sharedPreferences_loggedUser = mContext.getSharedPreferences("MyLogDetails", MODE_PRIVATE);
             Boolean paid_OR_not = sharedPreferences_loggedUser.getBoolean("paidStatus",false);
            if (paid_OR_not)
            {
                viewHolder.payLayout.setVisibility(View.GONE);
            }

            // viewHolder.cardView.setBackgroundResource(R.color.colorGreytrans);
        }
        viewHolder.tvName.setText(catNames.get(position));
        //downloading icon from webservice
       /* Picasso.with(convertView.getContext())
                .load("http://192.168.1.202:8012/Uploads/BookCategoryImages/" + catImages.get(position))
                .into(viewHolder.ivImage);*/

       // Log.d("ThePath","http://test.pcplglobal.com/Uploads/BookCategoryImages/" + catImages.get(position));
        Picasso.with(convertView.getContext())
                .load("http://book.splogics.com/Uploads/BookCategoryImages/" + catImages.get(position))
                .into(viewHolder.ivImage);

        return convertView;
    }

    static class ViewHolder {
        TextView tvName;
        ImageView ivImage;
        ImageView ivBackdiv;
        RelativeLayout payLayout;
        CardView cardView;
    }
}
