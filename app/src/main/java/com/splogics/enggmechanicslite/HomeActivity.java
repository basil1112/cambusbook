package com.splogics.enggmechanicslite;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.splogics.enggmechanicslite.Adapter.DrawerItemCustomAdapter;
import com.splogics.enggmechanicslite.Adapter.ObjectDrawerItem;
import com.splogics.enggmechanicslite.Fragments.AboutUS;
import com.splogics.enggmechanicslite.Fragments.AppRater;
import com.splogics.enggmechanicslite.Fragments.ContactFragment;
import com.splogics.enggmechanicslite.Fragments.FirstPageFragment;
import com.splogics.enggmechanicslite.Fragments.LogoutFragment;
import com.splogics.enggmechanicslite.Fragments.OtherApps;
import com.splogics.enggmechanicslite.Fragments.RateUs;
import com.splogics.enggmechanicslite.Fragments.TermsFragment;
import com.splogics.enggmechanicslite.Fragments.html_findReplace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    InterstitialAd mInterstitialAd;

    private DrawerLayout _mdrawerLayout;
    private ListView _mlistViewNavs;
    private String[] _mNavigationDrawerNavs;
    private String loggeduser_name = "", loggeduser_email = "";
    int flag = 0;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayList<Integer> _mnavigationDrawerIcons;
    Toolbar toolbar;
    public static final int icons_res[] =
    {
            R.mipmap.homeicon, R.mipmap.abouticon,R.mipmap.con_us,R.mipmap.otherapps,R.mipmap.rateus,R.mipmap.log_out,R.mipmap.terms_agree,R.mipmap.pay_ment
    };
    private TextView _txt_user;
    private SharedPreferences shared_LoggedUserDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Rate my app Starting

        AppRater.app_launched(this);


        //screenshot testing
       HomeActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        //loading ads

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8638167269558284/4458202456");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        requestNewInterstitial();

        //loading ads ends

        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_name);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag == 0) {
                    _mdrawerLayout.openDrawer(GravityCompat.START);
                    flag = 1;
                    showAdvs();
                } else {
                    flag = 0;
                    _mdrawerLayout.closeDrawer(GravityCompat.START);
                    showAdvs();
                }
            }
        });

        getSupportActionBar();

        _mNavigationDrawerNavs = getResources().getStringArray(R.array.navigation_drawer_navs_array);

        SharedPreferences sharedPreferences_loggedUser = HomeActivity.this.getSharedPreferences("MyLogDetails", MODE_PRIVATE);
        Boolean paid_OR_not = sharedPreferences_loggedUser.getBoolean("paidStatus", false);
        if (paid_OR_not)
        {
            List<String> menu_names = new ArrayList<String>(Arrays.asList(_mNavigationDrawerNavs));
            int index = menu_names.indexOf("Upgrade to pro version");
            menu_names.remove("Upgrade to pro version");
            _mNavigationDrawerNavs = menu_names.toArray(new String[0]);
        }

        ObjectDrawerItem[] drawerItem = new ObjectDrawerItem[_mNavigationDrawerNavs.length];
        for (int i = 0; i < _mNavigationDrawerNavs.length; i++) {
            drawerItem[i] = new ObjectDrawerItem(icons_res[i], _mNavigationDrawerNavs[i]);

        }
        //this is txtbox which shows logged users name
        _txt_user = (TextView) findViewById(R.id.Home_loggedusername);

        _mdrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        _mdrawerLayout.setScrimColor(ContextCompat.getColor(HomeActivity.this, android.R.color.transparent));

        _mlistViewNavs = (ListView) findViewById(R.id.left_sidebar_drawer);
        _mlistViewNavs.setAdapter(new DrawerItemCustomAdapter(HomeActivity.this, R.layout.listview_item_row, drawerItem));
        _mlistViewNavs.setOnItemClickListener(new DrawerItemClickListener());

        selectItem(0);


  /*  this is the details of the logged user which has been added at the time of
      sign up to the sharedpreference
      these datas are also stored in the server too using webservice */

        shared_LoggedUserDetails = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
        loggeduser_name = shared_LoggedUserDetails.getString("LoggedUser_Name", "null");

        //setting the logged user :P
        _txt_user.setText(loggeduser_name);


//       // TextView txttest=(TextView)findViewById(R.id.apptitle);
//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Chantelli_Antiqua.ttf");
//// Assign the typeface to the view
//       // txttest.setTypeface(font);


    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        Log.d("advs", "requested");
    }

    private void showAdvs() {
        // this is here were paid users are identified
        SharedPreferences sharedPreferences_loggedUser = getSharedPreferences("MyLogDetails", MODE_PRIVATE);
        Boolean paid_OR_not = sharedPreferences_loggedUser.getBoolean("paidStatus", false);
        if (!paid_OR_not) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        }
    }

    public class DrawerItemClickListener implements ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            position = position + 1;
            selectItem(position);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        HomeActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.d("Back", "BackPressed");
        }
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                HomeActivity.this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
                Window window = HomeActivity.this.getWindow();
                WindowManager wm = HomeActivity.this.getWindowManager();
                wm.removeViewImmediate(window.getDecorView());
                wm.addView(window.getDecorView(), window.getAttributes());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(HomeActivity.this, HomeActivity.class));
                    }
                }, 100);
            }
        }
        if((keyCode == KeyEvent.KEYCODE_HOME))
        {
            Toast.makeText(this, "You pressed the home button!", Toast.LENGTH_LONG).show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void selectItem(int position) {

        // THIS IS THE METHOD WHICH REPLACES THE FRAGMENT AND DIRECTIONS :)
        showAdvs();
        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new FirstPageFragment();
                break;
            case 1:
                fragment = new FirstPageFragment();
                break;

            case 2:
                fragment = new AboutUS();
                break;

            case 3:
                fragment = new ContactFragment();
                break;
            case 4:
                //contact us
                fragment = new OtherApps();
                break;
            case 5:
//                logout
                fragment = new RateUs();
                break;
            case 6:
                fragment = new LogoutFragment();
                break;

            case 7:
                fragment = new TermsFragment();
                break;
            case 8:
                fragment = new html_findReplace();
            default:
                break;
        }

        if (fragment != null) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            String backStateName = fragment.getClass().getName();
            if(position!=0)
            {
                ft.addToBackStack(backStateName);
            }
            ft.commit();
            _mdrawerLayout.closeDrawer(_mlistViewNavs);

        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_icons, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        return super.onOptionsItemSelected(item);

    }



    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }
        else
        {
            super.onBackPressed();
        }

        this.doubleBackToExitPressedOnce = true;
        View  viewer=this.findViewById(android.R.id.content).getRootView();
        Snackbar snackbar;
        snackbar=Snackbar.make(viewer,"Press Back Button twice to Exit",Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundResource(R.color.colorBlue_new);
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();


        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 500);

    }



}
